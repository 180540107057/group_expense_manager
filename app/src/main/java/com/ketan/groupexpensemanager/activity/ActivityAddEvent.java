package com.ketan.groupexpensemanager.activity;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;

import com.ketan.groupexpensemanager.R;
import com.ketan.groupexpensemanager.database.TblGroup;
import com.ketan.groupexpensemanager.database.TblTotalAmount;
import com.ketan.groupexpensemanager.model.GroupModel;
import com.ketan.groupexpensemanager.util.Constant;

import java.util.ArrayList;
import java.util.Calendar;

import de.hdodenhof.circleimageview.CircleImageView;
import soup.neumorphism.NeumorphButton;


public class ActivityAddEvent extends BaseActivity {
    NeumorphButton btnSaveEvent, btnCancleEvent;
    EditText etDatePicker, etEventName;
    CircleImageView uploadImage;
    String imagePath = null;
    ArrayList<GroupModel> arrayList;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_event);
        initFindId();
        setUpActionBar(getString(R.string.lbl_add_event), true);
        setDataToView();
        cancelButtonEventForAddGroup();
        clickEventOfDatePickerDialog();
        dataForSubmitOrUpdate();
        setUploadImages();
        arrayList = new TblGroup(getApplicationContext()).getGroupDetail();
    }


    void initFindId() {
        etEventName = findViewById(R.id.etEventName);
        etDatePicker = findViewById(R.id.etDatePicker);
        btnCancleEvent = findViewById(R.id.btnAddEventCancle);
        btnSaveEvent = findViewById(R.id.btnAddEventSave);
        uploadImage = findViewById(R.id.CivUploadGroupImage);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case 1:
                if (requestCode == Activity.RESULT_OK) {
                    Uri uri = data.getData();
                    if (uri != null) {
                        imagePath = uri.getPath();
                        uploadImage.setImageURI(uri);
                    }
                } else {
                    Toast.makeText(this, R.string.something_went_to_wrong, Toast.LENGTH_LONG).show();
                }
                break;


        }

    }

    private void setUploadImages() {
        uploadImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                ImagePicker.Companion.with(ActivityAddEvent.this)
//                        .crop()
//                        .cropOval()
//                        .start();
                showDialogForImagePicker();
            }
        });
    }


    private void dataForSubmitOrUpdate() {

        if (!(getIntent().hasExtra(Constant.UPDATE_USER_INTENT_NAME))) {
            submitButtonClickEventForInsertGroup();
        } else {
            UpdateButtonClickEventForUpdateGroup();
        }
    }

    private void UpdateButtonClickEventForUpdateGroup() {
        setUpActionBar(getString(R.string.lbl_update_event), true);
        btnSaveEvent.setText(R.string.btn_update);
        final GroupModel finalModel = (GroupModel) getIntent().getSerializableExtra(Constant.UPDATE_USER_INTENT_NAME);
        etEventName.setText(finalModel.getGroupName());
        etDatePicker.setText(finalModel.getDate());
        String imagePaths = finalModel.getImagePath();
        if (imagePath != null) {
            uploadImage.setImageURI(Uri.parse(imagePaths));
        }


        btnSaveEvent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean isGroupNameExists = false;
                ArrayList<GroupModel> arrayList1 = new TblGroup(getApplicationContext()).getGroupDetail();
                for (int i = 0; i < arrayList1.size(); i++) {
                    if (finalModel.getGroupName().equals(arrayList1.get(i).getGroupName())) {
                        continue;
                    }
                    if (etEventName.getText().toString().equals(arrayList1.get(i).getGroupName())) {

                        isGroupNameExists = true;
                        break;
                    }

                }
                if (!isGroupNameExists) {
                    if (isValidEventName()) {
                        long i = new TblGroup(getApplicationContext()).updateGroupByID(finalModel.getGroupID(), etEventName.getText().toString(),
                                etDatePicker.getText().toString(), imagePath);


                        if (i > 0) {
                            showToast(getApplicationContext(), getString(R.string.group_updated_sucess));
                            Intent intent = new Intent(ActivityAddEvent.this, ActivityMain.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);

                        } else {
                            showToast(getApplicationContext(), getString(R.string.something_went_to_wrong));
                        }
                    }
                } else {
                    etEventName.setError("Event Already Exists");
                }
            }
        });

    }


    private void clickEventOfDatePickerDialog() {
        etDatePicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar calendar = Calendar.getInstance();
                DatePickerDialog datePickerDialog = new DatePickerDialog(ActivityAddEvent.this, new OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        etDatePicker.setText(String.format("%02d", dayOfMonth) + "/" + String.format("%02d", month + 1) + "/" + year);
                    }
                },
                        calendar.get(Calendar.YEAR),
                        calendar.get(Calendar.MONTH),
                        calendar.get(Calendar.DAY_OF_MONTH));
                datePickerDialog.show();
            }
        });
    }


    private void cancelButtonEventForAddGroup() {

        btnCancleEvent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }

    private boolean isMemberNameNotRepeat() {
        boolean isGroupNameNotRepeat = false;
        ArrayList<GroupModel> groupListTemp = new TblGroup(getApplicationContext()).getGroupDetail();
        if (groupListTemp.size() > 0) {
            for (int i = 0; i < groupListTemp.size(); i++) {
                if (etEventName.getText().toString().equals(groupListTemp.get(i).getGroupName())) {
                    isGroupNameNotRepeat = true;
                    break;
                }
            }
        }
        return isGroupNameNotRepeat;

    }

    public void submitButtonClickEventForInsertGroup() {
        setUpActionBar(getString(R.string.lbl_add_event), true);
        btnSaveEvent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isMemberNameNotRepeat()) {
                    if (isValidEventName()) {
                        long i = new TblGroup(getApplicationContext()).insertGroup(etEventName.getText().toString()
                                , etDatePicker.getText().toString(), imagePath);
                        long j = new TblTotalAmount(getApplicationContext()).insertedGroupID();

                        if (i > 0 && j > 0) {

                            showToast(getApplicationContext(), "Event Created Successfully!");
                            Intent intent = new Intent(ActivityAddEvent.this, ActivityMain.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);

                        } else {
                            showToast(getApplicationContext(), "Something Went to Wrong!");
                        }
                    }
                } else {
                    etEventName.setError("Event Name Already Exists");
                }
            }
        });
    }
    void setDataToView() {
        final Calendar calendar = Calendar.getInstance();
        etDatePicker.setText(
                String.format("%02d", calendar.get(Calendar.DATE)) + "/" +
                        String.format("%02d", ((calendar.get(Calendar.MONTH)) + 1)) + "/" +
                        calendar.get(Calendar.YEAR));
    }


    boolean isValidEventName() {
        boolean isValid = true;
        if (TextUtils.isEmpty(etEventName.getText().toString().trim())) {
            isValid = false;
            etEventName.setError(getString(R.string.error_event_name));
        }
        return isValid;
    }

    private void showDialogForImagePicker() {
        AlertDialog.Builder builder = new AlertDialog.Builder(ActivityAddEvent.this);
        LayoutInflater inflater = getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_for_upload_image, null);
        builder.setCancelable(false);
        builder.setView(view);

        final AlertDialog alertDialog = builder.create();
        alertDialog.show();

        Button gallery = view.findViewById(R.id.btn1);
        Button camera = view.findViewById(R.id.btn2);

        gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                takeImageFromGallery();
                alertDialog.cancel();
            }
        });

        camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkAndRequestPermissionForCamera()) {
                    takeImageFromCamera();
                }
                alertDialog.cancel();

            }
        });
    }

    private void takeImageFromGallery() {
        Intent gallery = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(gallery, 1);
    }

    private void takeImageFromCamera() {
        Intent imageFromCamera = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (imageFromCamera.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(imageFromCamera, 2);
        }
    }

    private boolean checkAndRequestPermissionForCamera() {
        if (Build.VERSION.SDK_INT >= 23) {
            int cameraPermission = ActivityCompat.checkSelfPermission(ActivityAddEvent.this, Manifest.permission.CAMERA);
            if (cameraPermission == PackageManager.PERMISSION_DENIED) {
                ActivityCompat.requestPermissions(ActivityAddEvent.this, new String[]{Manifest.permission.CAMERA}, 20);
                return false;
            }
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 20 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            takeImageFromCamera();
        } else {
            Toast.makeText(this, R.string.error_permission_is_not_given, Toast.LENGTH_LONG).show();
        }

    }
}