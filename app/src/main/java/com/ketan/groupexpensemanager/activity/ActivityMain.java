package com.ketan.groupexpensemanager.activity;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ketan.groupexpensemanager.R;
import com.ketan.groupexpensemanager.adapter.GroupAdapter;
import com.ketan.groupexpensemanager.database.TblGroup;
import com.ketan.groupexpensemanager.database.TblMember;
import com.ketan.groupexpensemanager.database.TblTotalAmount;
import com.ketan.groupexpensemanager.database.TblTransaction;
import com.ketan.groupexpensemanager.model.GroupModel;
import com.ketan.groupexpensemanager.util.Constant;

import java.util.ArrayList;
import java.util.List;

import soup.neumorphism.NeumorphFloatingActionButton;


public class ActivityMain extends BaseActivity implements GroupAdapter.OnItemClickListenerForRecyclerView {

    static int groupId;
    public static final int REQUEST_ID_MULTIPLE_PERMISSIONS = 1;
    static String groupName;
    NeumorphFloatingActionButton floatingActionButton;
    TextView tvEventNotFound;
    ArrayList<GroupModel> groupDetail = new ArrayList<>();
    GroupAdapter groupAdapter;
    RecyclerView rcvGroupList;
    MyBroadCastForReceivingTransactionUpdate myBroadCastForReceivingTransactionUpdate = new MyBroadCastForReceivingTransactionUpdate();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initFindViewById();
        setUpActionBar(getString(R.string.app_name), false);
        clickEventFloatingActionButton();
        setAdapter();
        LocalBroadcastManager.getInstance(getApplicationContext()).registerReceiver(myBroadCastForReceivingTransactionUpdate, new IntentFilter(Constant.BROADCAST_FOR_UPDATE_TRANSACTION));
        checkAndRequestPermissions();

    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        LocalBroadcastManager.getInstance(getApplicationContext()).unregisterReceiver(myBroadCastForReceivingTransactionUpdate);
    }

    //inflating MenuBar(share , Developer) option
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menubar, menu);
        return true;
    }

    // if want close app than show conformation message
    @Override
    public void onBackPressed() {
        new AlertDialog.Builder(this)
                .setTitle(R.string.exit_from_app)
                .setMessage(R.string.sure_want_to_exit)
                .setNegativeButton(android.R.string.no, null)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface arg0, int arg1) {
                        ActivityMain.super.onBackPressed();
                    }
                }).create().show();
    }

    // click event of individual group
    @Override
    public void onClick(int position) {
        Intent intent = new Intent(this, ActivityGroupDetail.class);
        this.groupId = groupDetail.get(position).getGroupID();
        this.groupName = groupDetail.get(position).getGroupName();
        startActivity(intent);
    }

    // LongClick event of individual group and open pop menu option with delete, update;
    @Override
    public void onLongClick(int position, View view) {
        showPopUpMenu(position, view);
    }

    //method for find Id of View
    public void initFindViewById() {
        floatingActionButton = findViewById(R.id.fabAddGroup);
        rcvGroupList = findViewById(R.id.rcvGroupList);
        tvEventNotFound = findViewById(R.id.tvNoEventFound);
    }

    // if data no found than show data no found TextView
    private void setNoEventFound() {
        if (!(groupDetail.size() > 0)) {
            rcvGroupList.setVisibility(View.GONE);
            tvEventNotFound.setVisibility(View.VISIBLE);

        }
    }

    //click event for floating action button
    void clickEventFloatingActionButton() {
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ActivityMain.this, ActivityAddEvent.class);
                startActivity(intent);
            }
        });
    }


    //set Adapter for group list
    void setAdapter() {

        groupDetail.addAll(new TblGroup(ActivityMain.this).getGroupDetail());
        setNoEventFound();
        groupAdapter = new GroupAdapter(groupDetail, this, this);
        rcvGroupList.setAdapter(groupAdapter);
        rcvGroupList.setLayoutManager(new LinearLayoutManager(this));
    }


    // show pop menu on long click and give option for update, delete
    private void showPopUpMenu(final int position, View v) {
        PopupMenu popupMenu = new PopupMenu(this, v);
        popupMenu.inflate(R.menu.popmenu_for_update_delete);
        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.deleteGroup:
                        deleteGroupByID(position);
                        return true;
                    case R.id.updateGroup:
                        openUpdateActivity(position);
                        return true;
                    default:
                        return false;
                }
            }
        });
        popupMenu.show();
    }


    // method for deleting group or event
    private void deleteGroupByID(int position) {

        int group_ID = groupDetail.get(position).getGroupID();
        long i = new TblGroup(this).deleteGroupByIdInTblGroup(group_ID);
        long j = new TblMember(this).deleteGroupByIdInTblMember(group_ID);
        long k = new TblTransaction(this).deleteGroupByIdInTblTransaction(group_ID);
        long l = new TblTotalAmount(this).deleteGroupByIdInTblTotalAmount(group_ID);
        if (i > 0) {
            Toast.makeText(this, R.string.group_deleted_sucessfully, Toast.LENGTH_SHORT).show();
            groupDetail.remove(position);
            groupAdapter.notifyDataSetChanged();
            setNoEventFound();
        } else {
            Toast.makeText(this, R.string.something_went_to_wrong, Toast.LENGTH_SHORT).show();
        }
    }

    // open detail of activity when click of update any group
    public void openUpdateActivity(int position) {
        Intent intent = new Intent(ActivityMain.this, ActivityAddEvent.class);
        intent.putExtra(Constant.UPDATE_USER_INTENT_NAME, groupDetail.get(position));
        startActivity(intent);
    }

    // all permission request which is needed for app
    private boolean checkAndRequestPermissions() {
        int externalStorage = ContextCompat.checkSelfPermission(getApplicationContext(),
                Manifest.permission.WRITE_EXTERNAL_STORAGE);
        int readStorage = ContextCompat.checkSelfPermission(getApplicationContext(),
                Manifest.permission.READ_EXTERNAL_STORAGE);

        int camera = ContextCompat.checkSelfPermission(getApplicationContext(),
                Manifest.permission.CAMERA);

        int contactPermission = ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.READ_CONTACTS);
        List<String> listPermissionsNeeded = new ArrayList<>();
        if (contactPermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.READ_CONTACTS);
        }
        if (externalStorage != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
        if (readStorage != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.READ_EXTERNAL_STORAGE);
        }
        if (camera != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.CAMERA);
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(this, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), REQUEST_ID_MULTIPLE_PERMISSIONS);
            return false;
        }
        return true;
    }

    //method which return current event^s group id
    public int getGroupId() {
        return groupId;
    }

    //method which return current event^s group id
    public String getGroupName() {
        return groupName;
    }

    // BroadCast receiver
    private class MyBroadCastForReceivingTransactionUpdate extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            groupDetail.clear();
            setAdapter();
        }
    }

}