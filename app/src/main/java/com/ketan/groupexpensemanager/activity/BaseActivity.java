package com.ketan.groupexpensemanager.activity;


import android.content.Context;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.ketan.groupexpensemanager.R;

public class BaseActivity extends AppCompatActivity {

    Toolbar toolbar;

    // method for setup action_bar
    void setUpActionBar(String actionBar_title, boolean isUpButton) {
        toolbar = findViewById(R.id.custom_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(actionBar_title);
        getSupportActionBar().setDisplayHomeAsUpEnabled(isUpButton);

    }

    void showToast(Context context, String Message) {
        Toast.makeText(context, Message, Toast.LENGTH_SHORT).show();
    }

}
