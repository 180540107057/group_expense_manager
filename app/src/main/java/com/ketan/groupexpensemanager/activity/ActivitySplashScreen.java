package com.ketan.groupexpensemanager.activity;

import android.content.Intent;
import android.os.Bundle;

import com.ketan.groupexpensemanager.R;

public class ActivitySplashScreen extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        //calling method for splash screen timer
        timerSplashScreen();
    }


    //method for pausing splash screen for five seconds.
    public void timerSplashScreen() {
        Thread td = new Thread() {
            @Override
            public void run() {
                try {
                    sleep(5000);
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    Intent intent = new Intent(ActivitySplashScreen.this, ActivityMain.class);
                    startActivity(intent);
                    finish();
                }
            }
        };
        td.start();

    }
}