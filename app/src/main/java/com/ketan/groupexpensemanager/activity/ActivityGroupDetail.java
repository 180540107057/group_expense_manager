package com.ketan.groupexpensemanager.activity;

import android.Manifest;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.Settings;
import android.text.format.DateFormat;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;
import com.itextpdf.kernel.colors.ColorConstants;
import com.itextpdf.kernel.colors.DeviceRgb;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.kernel.pdf.canvas.draw.SolidLine;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.element.Cell;
import com.itextpdf.layout.element.LineSeparator;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.element.Table;
import com.itextpdf.layout.property.TextAlignment;
import com.ketan.groupexpensemanager.R;
import com.ketan.groupexpensemanager.database.TblMember;
import com.ketan.groupexpensemanager.database.TblTotalAmount;
import com.ketan.groupexpensemanager.fragment.MemberFragment;
import com.ketan.groupexpensemanager.fragment.ReportFragment;
import com.ketan.groupexpensemanager.fragment.TransactionFragment;
import com.ketan.groupexpensemanager.model.MemberModel;
import com.ketan.groupexpensemanager.model.TotalAmountModel;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Date;


public class ActivityGroupDetail extends AppCompatActivity {
    static int pagerValue;
    static String groupName;
    ViewPager viewPager;
    TabLayout tabLayout;
    GroupDetailPagerAdapter groupDetailPagerAdapter;
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_group_detail);
        this.groupName = new ActivityMain().getGroupName();
        initFindId();
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(groupName);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setPagerAdapter();
        setViewPagersTitle(); //for show title for viewpager
        this.pagerValue = getIntent().getIntExtra("VIEW_PAGER_PAGE", 0);
        viewPager.setCurrentItem(pagerValue);
    }


    private void initFindId() {
        viewPager = findViewById(R.id.VpViewPager);
        tabLayout = findViewById(R.id.TabForDisplayTitle);
        toolbar = findViewById(R.id.toolbar);
    }

    // show pdf icon in menuBar
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menubar_for_pdf, menu);
        return true;
    }

    private void setViewPagersTitle() {
        tabLayout.setupWithViewPager(viewPager);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.pdf:
                try {
                    if (checkPermission()) {
                        generatePdf();
                    } else {
                        requestPermission();
                    }

                } catch (FileNotFoundException e) {
                    Toast.makeText(this, R.string.file_not_found, Toast.LENGTH_LONG).show();
                }
                openPdf();

            default:
                return super.onOptionsItemSelected(item);
        }

    }

    //method which generate pdf
    private void generatePdf() throws FileNotFoundException {
        //create folder
        File file = new File(Environment.getExternalStorageDirectory() + "/Group Expense Manager");
        if (!file.mkdirs()) {
            file.mkdirs();
        }


        String path = file.toString();
        File file1 = new File(path, "Report.pdf");
        PdfWriter pdfWriter = new PdfWriter(file1);
        PdfDocument pdfDocument = new PdfDocument(pdfWriter);
        Document document = new Document(pdfDocument);

        int groupId = new ActivityMain().getGroupId();
        int totalMember = new TblMember(getApplicationContext()).getTotalNumberOfMemberByGroupId(groupId);
        TotalAmountModel totalAmountModel = new TblTotalAmount(getApplicationContext()).getTotalAmountDetail(groupId);
        int totalIncomeAmount = totalAmountModel.getTotalIncomeAmount();
        int totalExpenseAmount = totalAmountModel.getTotalExpenseAmount();
        int expensePerHead = 0;
        ArrayList<MemberModel> memberList = new TblMember(getApplicationContext()).getMemberByID(groupId);
        if (totalMember > 0) {
            expensePerHead = totalExpenseAmount / totalMember;
        }

        String groupName = new ActivityMain().getGroupName();
        Date d = new Date();
        String date = (String) DateFormat.format("MMMM d, yyyy ", d.getTime());


        float eventRaw[] = {500f};
        Table tableForEventName = new Table(eventRaw);
        Cell eventName = new Cell();
        eventName.add(new Paragraph("Event" + "  :  " + groupName).setBold().setTextAlignment(TextAlignment.CENTER).setFontSize(30));
        eventName.setBorder(null);
        tableForEventName.addCell(eventName);

        SolidLine line = new SolidLine(1f);
        LineSeparator ls = new LineSeparator(line);
        ls.setMarginTop(5);
        ls.setMarginBottom(5);

        float totalMemberRaw[] = {120f, 100f, 180f, 120f, 100f};
        Table tableForTotalMember = new Table(totalMemberRaw);
        Cell totalNumber1 = new Cell();
        Cell totalNumber2 = new Cell();
        Cell totalNumber3 = new Cell();
        Cell totalNumber4 = new Cell();
        Cell totalNumber5 = new Cell();
        totalNumber1.setBorder(null);
        totalNumber2.setBorder(null);
        totalNumber3.setBorder(null);
        totalNumber4.setBorder(null);
        totalNumber5.setBorder(null);
        Paragraph totalNumberLabel = new Paragraph("Total Member");
        totalNumber1.add(totalNumberLabel);
        Paragraph totalNumberValue = new Paragraph(": " + totalMember);
        totalNumber2.add(totalNumberValue);
        totalNumber3.add(new Paragraph("  "));
        totalNumber4.add(new Paragraph("Generated Date: "));
        totalNumber5.add(new Paragraph(" " + date));
        tableForTotalMember.addCell(totalNumber1);
        tableForTotalMember.addCell(totalNumber2);
        tableForTotalMember.addCell(totalNumber3);
        tableForTotalMember.addCell(totalNumber4);
        tableForTotalMember.addCell(totalNumber5);
        tableForTotalMember.setBorder(null);

        Cell totalIncome1 = new Cell();
        Cell totalIncome2 = new Cell();

        totalIncome1.setBorder(null);
        totalIncome2.setBorder(null);

        Paragraph totalIncomeLabel = new Paragraph("Total Income");
        totalIncome1.add(totalIncomeLabel);
        Paragraph totalIncomeValue = new Paragraph(": " + totalIncomeAmount);
        totalIncome2.add(totalIncomeValue);

        tableForTotalMember.addCell(totalIncome1);
        tableForTotalMember.addCell(totalIncome2);


        Cell totalExpense1 = new Cell();
        Cell totalExpense2 = new Cell();
        totalExpense1.setBorder(null);
        totalExpense2.setBorder(null);
        Paragraph totalExpenseLabel = new Paragraph("Total Expense");
        totalIncome1.add(totalExpenseLabel);
        Paragraph totalExpenseValue = new Paragraph(": " + totalExpenseAmount);
        totalIncome2.add(totalExpenseValue);
        tableForTotalMember.addCell(totalExpense1);
        tableForTotalMember.addCell(totalExpense2);


        Cell expensePerHead1 = new Cell();
        Cell expensePerHead2 = new Cell();
        expensePerHead1.setBorder(null);
        expensePerHead2.setBorder(null);
        Paragraph expensePerHeadLabel = new Paragraph("Expense Per Head");
        totalIncome1.add(expensePerHeadLabel);
        Paragraph expensePerHeadValue = new Paragraph(": " + expensePerHead);
        totalIncome2.add(expensePerHeadValue);
        tableForTotalMember.addCell(expensePerHead1);
        tableForTotalMember.addCell(expensePerHead2);


        float reportRaw[] = {120f, 100f, 100f, 100f, 100f};
        Table tableReport = new Table(reportRaw);
        Cell memberName = new Cell();
        memberName.setBackgroundColor(new DeviceRgb(246, 95, 66));
        Paragraph memberNamePara = new Paragraph("MEMBER NAME").setBold().setTextAlignment(TextAlignment.CENTER).setFontColor(ColorConstants.WHITE);
        memberName.add(memberNamePara);

        Cell income = new Cell();
        income.setBackgroundColor(new DeviceRgb(246, 95, 66));
        Paragraph incomePara = new Paragraph("INCOME").setBold().setTextAlignment(TextAlignment.CENTER).setFontColor(ColorConstants.WHITE);
        income.add(incomePara);

        Cell expense = new Cell();
        expense.setBackgroundColor(new DeviceRgb(246, 95, 66));
        Paragraph expensePara = new Paragraph("EXPENSE").setBold().setTextAlignment(TextAlignment.CENTER).setFontColor(ColorConstants.WHITE);
        expense.add(expensePara);

        Cell refund = new Cell();
        refund.setBackgroundColor(new DeviceRgb(246, 95, 66));
        Paragraph refundPara = new Paragraph("REFUND").setBold().setTextAlignment(TextAlignment.CENTER).setFontColor(ColorConstants.WHITE);
        refund.add(refundPara);

        Cell owns = new Cell();
        owns.setBackgroundColor(new DeviceRgb(246, 95, 66));
        Paragraph ownsPara = new Paragraph("OWNS").setBold().setTextAlignment(TextAlignment.CENTER).setFontColor(ColorConstants.WHITE);
        owns.add(ownsPara);

        tableReport.addCell(memberName);
        tableReport.addCell(income);
        tableReport.addCell(expense);
        tableReport.addCell(refund);
        tableReport.addCell(owns);

        for (int i = 0; i < totalMember; i++) {
            MemberModel memberModel = memberList.get(i);
            String member_name = memberModel.getMemberName();
            int member_income = memberModel.getIncomeAmount();
            int member_expense = memberModel.getExpenseAmount();
            int member_refund = 0, member_owns = 0;
            if (member_expense >= expensePerHead) {
                member_refund = member_expense - expensePerHead;
            } else {
                member_owns = expensePerHead - member_expense;
            }

            Cell cell1 = new Cell();
            Paragraph p1 = new Paragraph(member_name).setTextAlignment(TextAlignment.CENTER);
            cell1.add(p1);

            Cell cell2 = new Cell();
            Paragraph p2 = new Paragraph(member_income + " ").setTextAlignment(TextAlignment.CENTER);
            cell2.add(p2);

            Cell cell3 = new Cell();
            Paragraph p3 = new Paragraph(member_expense + " ").setTextAlignment(TextAlignment.CENTER);
            cell3.add(p3);

            Cell cell4 = new Cell();
            Paragraph p4 = new Paragraph(member_refund + " ").setTextAlignment(TextAlignment.CENTER);
            cell4.add(p4);

            Cell cell5 = new Cell();
            Paragraph p5 = new Paragraph(member_owns + " ").setTextAlignment(TextAlignment.CENTER);
            cell5.add(p5);

            tableReport.addCell(cell1);
            tableReport.addCell(cell2);
            tableReport.addCell(cell3);
            tableReport.addCell(cell4);
            tableReport.addCell(cell5);

        }

        document.add(tableForEventName);
        document.add(ls);
        document.add(tableForTotalMember);
        document.add(tableReport);
        document.close();

        Toast.makeText(this, R.string.pdf_genered_sucessfullly, Toast.LENGTH_LONG).show();
    }

    private void openPdf() {
        File pdfFile = new File(Environment.getExternalStorageDirectory() + "/Group Expense Manager/" + "Report.pdf");  // -> filename = manual.pdf

        Uri excelPath;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {

            excelPath = FileProvider.getUriForFile(this, getApplicationContext().getPackageName() + ".provider", pdfFile);
        } else {
            excelPath = Uri.fromFile(pdfFile);
        }
        Intent pdfIntent = new Intent(Intent.ACTION_VIEW);
        pdfIntent.setDataAndType(excelPath, "application/pdf");
        pdfIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        pdfIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        pdfIntent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        try {
            startActivity(pdfIntent);
        } catch (ActivityNotFoundException e) {
            Toast.makeText(this, R.string.error_no_app_avilable_for_open_pdf, Toast.LENGTH_SHORT).show();
        }
    }

    private void setPagerAdapter() {
        groupDetailPagerAdapter = new GroupDetailPagerAdapter(getSupportFragmentManager());
        viewPager.setAdapter(groupDetailPagerAdapter);
        viewPager.setOffscreenPageLimit(3);
    }

    boolean checkPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            return Environment.isExternalStorageManager();
        } else {
            int checkRead = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE);
            int checkWrite = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
            return checkRead == PackageManager.PERMISSION_GRANTED && checkWrite == PackageManager.PERMISSION_GRANTED;
        }
    }

    void requestPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            try {
                Intent intent = new Intent(Settings.ACTION_MANAGE_ALL_FILES_ACCESS_PERMISSION);
                intent.addCategory("android.intent.category.DEFAULT");
                intent.setData(Uri.parse(String.format("package:%s", new Object[]{getApplicationContext().getPackageName()})));
                startActivity(intent);
            } catch (Exception e) {
                Intent obj = new Intent();
                obj.setAction(Settings.ACTION_MANAGE_ALL_FILES_ACCESS_PERMISSION);
                startActivity(obj);
            }
        } else {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE}, 200);
        }
    }


    //class of pagerAdapter for the ViewPager
    public class GroupDetailPagerAdapter extends FragmentPagerAdapter {


        public GroupDetailPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @NonNull
        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    return new MemberFragment();
                case 1:
                    return new TransactionFragment();
                case 2:
                    return new ReportFragment();
                default:
                    return null;

            }

        }

        @Override
        public int getCount() {
            return 3;
        }

        @Nullable
        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return getResources().getText(R.string.member_fragment);
                case 1:
                    return getResources().getText(R.string.transaction_fragment);
                case 2:
                    return getResources().getText(R.string.report_fragment);

            }
            return null;
        }
    }
}