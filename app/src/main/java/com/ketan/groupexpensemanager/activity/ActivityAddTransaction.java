package com.ketan.groupexpensemanager.activity;


import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.google.android.material.appbar.MaterialToolbar;
import com.ketan.groupexpensemanager.R;
import com.ketan.groupexpensemanager.adapter.SpinnerAdapterForShowingMemberList;
import com.ketan.groupexpensemanager.database.TblMember;
import com.ketan.groupexpensemanager.database.TblTotalAmount;
import com.ketan.groupexpensemanager.database.TblTransaction;
import com.ketan.groupexpensemanager.model.MemberModel;
import com.ketan.groupexpensemanager.model.TotalAmountModel;
import com.ketan.groupexpensemanager.model.TransactionModel;
import com.ketan.groupexpensemanager.util.Constant;

import java.util.ArrayList;
import java.util.Calendar;

import soup.neumorphism.NeumorphButton;

public class ActivityAddTransaction extends BaseActivity {
    static int k;
    static int previousTransactionAmount;
    static int previousIncomeOrIncome;
    static int transactionId;
    static int previousMemberId;
    ArrayList<MemberModel> memberListForSpinner = new ArrayList<>();
    Spinner spinnerForShowMemberList;
    RadioButton isExpense, isIncome;
    SpinnerAdapterForShowingMemberList sc;
    MaterialToolbar toolbar;
    NeumorphButton submitButtonForAddTransaction, cancelButtonForAddTransaction;
    EditText editTextForTakingValueOfAmountOfTransaction, datePicker, etDescription;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_transaction);
        initFindId();
        this.k = new ActivityMain().getGroupId();
        setAdapterForSpinnerToShowMemberList(k);
        buttonCancelClickEvent();
        clickEventOfDatePickerDialog();
        setDataToView();
        IsUpdateForTransactionForNewTransaction();
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("ADD TRANSACTION");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    public void initFindId() {
        isIncome = findViewById(R.id.rbIncome);
        isExpense = findViewById(R.id.rbExpense);
        spinnerForShowMemberList = findViewById(R.id.sp_for_showing_memberList);
        submitButtonForAddTransaction = findViewById(R.id.btnForSubmitTransactionDetail);
        cancelButtonForAddTransaction = findViewById(R.id.btnForCancelTransactionDetail);
        editTextForTakingValueOfAmountOfTransaction = findViewById(R.id.etForEnterTransactionAmount);
        datePicker = findViewById(R.id.etDatePickerForTransaction);
        toolbar = findViewById(R.id.toolbar_for_add_transaction);
        etDescription = findViewById(R.id.etDescription);
    }

    public void setAdapterForSpinnerToShowMemberList(int group_id) {
        memberListForSpinner.addAll(new TblMember(this).getMemberByID(group_id));
        this.sc = new SpinnerAdapterForShowingMemberList(memberListForSpinner, this);
        spinnerForShowMemberList.setAdapter(sc);
    }


    // click event of submit transaction detail
    public void clickEventOfAddTransactionSubmitButton(final int group_id) {
        submitButtonForAddTransaction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isValidEventName()) {
                    String value = editTextForTakingValueOfAmountOfTransaction.getText().toString();
                    int finalValue = Integer.parseInt(value);
                    if (finalValue > 0) {
                        int memberID = memberListForSpinner.get(spinnerForShowMemberList.getSelectedItemPosition()).getMemberID();
                        long i = new TblTransaction(getApplication()).insertTransactionDetail(finalValue,
                                group_id, memberID,
                                memberListForSpinner.get(spinnerForShowMemberList.getSelectedItemPosition()).getMemberName(), isExpense.isChecked() ? 1 : 0, datePicker.getText().toString(), etDescription.getText().toString());

                        //set income and expense value in variable
                        int incomeAmountForTblMember = memberListForSpinner.get(spinnerForShowMemberList.getSelectedItemPosition()).getIncomeAmount() +
                                (isExpense.isChecked() ? 0 : finalValue);
                        int expenseAmountForTblMember = memberListForSpinner.get(spinnerForShowMemberList.getSelectedItemPosition()).getExpenseAmount() +
                                (isExpense.isChecked() ? finalValue : 0);


                        //calling the method of updating income and expenseAmount

                        long j = new TblMember(getApplication()).insertAmountToMemberAccount(incomeAmountForTblMember, expenseAmountForTblMember, memberID);

                        int totalIncomeAmount = new TblTotalAmount(getApplicationContext()).getTotalAmountDetail(group_id).getTotalIncomeAmount() +
                                (isExpense.isChecked() ? 0 : finalValue);
                        int totalExpenseAmount = new TblTotalAmount(getApplicationContext()).getTotalAmountDetail(group_id).getTotalExpenseAmount() +
                                (isExpense.isChecked() ? finalValue : 0);

                        long k = new TblTotalAmount(getApplicationContext()).insertTotalAmount(totalIncomeAmount, totalExpenseAmount, group_id);


                        if (i > 0 && j > 0 && k > 0) {
                            Toast.makeText(getApplicationContext(), "Transaction Added ", Toast.LENGTH_SHORT).show();
                            LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(new Intent(Constant.BROADCAST_FOR_UPDATE_TRANSACTION));
                            Intent intent = new Intent(ActivityAddTransaction.this, ActivityGroupDetail.class);
                            intent.putExtra("VIEW_PAGER_PAGE", 1);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                        } else {
                            Toast.makeText(getApplicationContext(), "Something went to Wrong", Toast.LENGTH_SHORT).show();
                        }

                    } else {
                        editTextForTakingValueOfAmountOfTransaction.setError("Amount Must Be Greater Than 0");
                    }
                }
            }

        });
    }

    public void buttonCancelClickEvent() {
        cancelButtonForAddTransaction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }

    public void IsUpdateForTransactionForNewTransaction() {
        if (getIntent().hasExtra(Constant.UPDATE_TRANSACTION_INTENT_NAME)) {
            clickEventForUpdateTransaction();

        } else {
            clickEventOfAddTransactionSubmitButton(k);
        }
    }


    public void clickEventForUpdateTransaction() {
        submitButtonForAddTransaction.setText(getString(R.string.btn_update));
        TransactionModel transactionModel = (TransactionModel) getIntent().getSerializableExtra(Constant.UPDATE_TRANSACTION_INTENT_NAME);
        this.previousTransactionAmount = transactionModel.getTransactionAmount();
        this.previousIncomeOrIncome = transactionModel.isExpense();
        this.previousMemberId = transactionModel.getMemberID();
        this.transactionId = transactionModel.getTransactionID();
        editTextForTakingValueOfAmountOfTransaction.setText(previousTransactionAmount + "");
        submitButtonForAddTransaction.setText("UPDATE");
        etDescription.setText(transactionModel.getDiscription());
        spinnerForShowMemberList.setSelection(returnPositionOfSelectedSpinnerPosition(transactionModel.getMemberID()));
        if (previousIncomeOrIncome == 1) {
            isExpense.setChecked(true);
        } else {
            isIncome.setChecked(true);
        }
        submitButtonForAddTransaction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int value = Integer.parseInt(editTextForTakingValueOfAmountOfTransaction.getText().toString());
                if (value > 0) {
                    int updatedTransactionAmount = Integer.parseInt(editTextForTakingValueOfAmountOfTransaction.getText().toString());
                    int updatedIncomeOrExpense = isExpense.isChecked() ? 1 : 0;
                    int updatedMemberId = memberListForSpinner.get(spinnerForShowMemberList.getSelectedItemPosition()).getMemberID();
                    if (previousIncomeOrIncome == updatedIncomeOrExpense && previousTransactionAmount == updatedTransactionAmount && previousMemberId == updatedMemberId) {
                        long lastUpdatedTransactionDate = new TblTransaction(getApplicationContext()).updateTransactionDateByTransactionId(transactionId, datePicker.getText().toString());
                        Toast.makeText(getApplicationContext(), lastUpdatedTransactionDate > 0 ? "Transaction Updated SuccessFully" : "Something Went Wrong", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(ActivityAddTransaction.this, ActivityGroupDetail.class);
                        LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(new Intent(Constant.BROADCAST_FOR_UPDATE_TRANSACTION));
                        intent.putExtra("VIEW_PAGER_PAGE", 1);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                    } else {
                        long lastUpdatedTransaction = new TblTransaction(getApplicationContext()).updateTransactionByTransactionId(transactionId,
                                memberListForSpinner.get(spinnerForShowMemberList.getSelectedItemPosition()).getMemberID(),
                                memberListForSpinner.get(spinnerForShowMemberList.getSelectedItemPosition()).getMemberName(),
                                isExpense.isChecked() ? 1 : 0, Integer.parseInt(editTextForTakingValueOfAmountOfTransaction.getText().toString()), datePicker.getText().toString(), etDescription.getText().toString());
                        long lastUpdatedTotalTransaction = updateTotalAmountTable(previousIncomeOrIncome, updatedIncomeOrExpense,
                                previousTransactionAmount, updatedTransactionAmount);
                        long lastUpdatedMemberIncomeOrExpense = updateMemberSIncomeOrExpenseWhenTransactionUpdated(previousMemberId, updatedMemberId, previousTransactionAmount,
                                updatedTransactionAmount, previousIncomeOrIncome, updatedIncomeOrExpense);
                        if (lastUpdatedTransaction > 0 && lastUpdatedTotalTransaction > 0) {
                            Toast.makeText(getApplicationContext(), R.string.transcation_update_sucessfully, Toast.LENGTH_SHORT).show();
                            LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(new Intent(Constant.BROADCAST_FOR_UPDATE_TRANSACTION));
                            Intent intent = new Intent(ActivityAddTransaction.this, ActivityGroupDetail.class);
                            intent.putExtra("VIEW_PAGER_PAGE", 1);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                        } else {

                            Toast.makeText(getApplicationContext(), R.string.something_went_to_wrong, Toast.LENGTH_SHORT).show();
                        }

                    }

                } else {
                    editTextForTakingValueOfAmountOfTransaction.setError(getString(R.string.error_transaction_must_be_greater_than_0));
                }
            }

        });

    }

    private void clickEventOfDatePickerDialog() {
        datePicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar calendar = Calendar.getInstance();
                DatePickerDialog datePickerDialog = new DatePickerDialog(ActivityAddTransaction.this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        datePicker.setText(String.format("%02d", dayOfMonth) + "/" + String.format("%02d", month + 1) + "/" + year);
                    }
                },
                        calendar.get(Calendar.YEAR),
                        calendar.get(Calendar.MONTH),
                        calendar.get(Calendar.DAY_OF_MONTH));
                datePickerDialog.show();
            }
        });
    }



    protected int returnPositionOfSelectedSpinnerPosition(int MemberId) {
        int position = 0;
        for (int i = 0; i < memberListForSpinner.size(); i++) {
            if (memberListForSpinner.get(i).getMemberID() == MemberId) {
                position = i;
                break;
            }
        }
        return position;

    }


    // logic for update total group in TblTotalAmount when updating transaction detail
    protected long updateTotalAmountTable(int previousIncomeORExpense, int updatedIncomeExpense,
                                          int previousTransactedAmount, int updatedTransactionAmount) {
        long lastUpdatedTotalAmountRaw = 0;
        TotalAmountModel totalAmountModel = new TblTotalAmount(getApplicationContext()).getTotalAmountDetail(k);
        int totalIncomeAmount = totalAmountModel.getTotalIncomeAmount();
        int totalExpenseAmount = totalAmountModel.getTotalExpenseAmount();
        if (previousIncomeORExpense != updatedIncomeExpense && previousTransactedAmount == updatedTransactionAmount) {
            if (previousIncomeORExpense == 0) {
                totalIncomeAmount = totalIncomeAmount - previousTransactionAmount;
                totalExpenseAmount = totalExpenseAmount + updatedTransactionAmount;
            } else {
                totalExpenseAmount = totalExpenseAmount - previousTransactionAmount;
                totalIncomeAmount = totalIncomeAmount + updatedTransactionAmount;
            }

            lastUpdatedTotalAmountRaw = new TblTotalAmount(getApplicationContext()).updateTotalTransactionDetail(k, totalIncomeAmount, totalExpenseAmount);

        } else if (previousIncomeORExpense == updatedIncomeExpense && previousTransactedAmount != updatedTransactionAmount) {
            if (previousIncomeORExpense == 1) {
                totalExpenseAmount = totalExpenseAmount - previousTransactedAmount;
                totalExpenseAmount = totalExpenseAmount + updatedTransactionAmount;
            } else {
                totalIncomeAmount = totalIncomeAmount - previousTransactedAmount;
                totalIncomeAmount = totalIncomeAmount + updatedTransactionAmount;
            }
            lastUpdatedTotalAmountRaw = new TblTotalAmount(getApplicationContext()).updateTotalTransactionDetail(k, totalIncomeAmount, totalExpenseAmount);
        } else if (previousIncomeORExpense != updatedIncomeExpense && previousTransactedAmount != updatedTransactionAmount) {
            if (previousIncomeORExpense == 0) {
                totalIncomeAmount = totalIncomeAmount - previousTransactedAmount;
                totalExpenseAmount = totalExpenseAmount + updatedTransactionAmount;

            } else {
                totalExpenseAmount = totalExpenseAmount - previousTransactedAmount;
                totalIncomeAmount = totalIncomeAmount + updatedTransactionAmount;
            }
            lastUpdatedTotalAmountRaw = new TblTotalAmount(getApplicationContext()).updateTotalTransactionDetail(k, totalIncomeAmount, totalExpenseAmount);

        } else if (previousIncomeORExpense == updatedIncomeExpense && previousTransactedAmount == updatedTransactionAmount) {
            lastUpdatedTotalAmountRaw = 1;
        }

        return lastUpdatedTotalAmountRaw;

    }


    // logic for update member detail in TblMember when updating transaction detail
    private long updateMemberSIncomeOrExpenseWhenTransactionUpdated(int previousMemberId, int updatedMemberId,
                                                                    int previousTransactionAmount, int updatedTransactionAmount,
                                                                    int previousIncomeOrExpense, int updatedIncomeOrExpense) {
        long lastUpdatedMemberIncomeOrExpense = 0;
        if (previousMemberId == updatedMemberId) {
            MemberModel memberModel = new TblMember(getApplicationContext()).getMemberModelByMemberId(previousMemberId);
            if (previousTransactionAmount != updatedTransactionAmount && previousIncomeOrExpense == updatedIncomeOrExpense) {
                if (previousIncomeOrExpense == 1) {
                    int newExpenseAmount;
                    newExpenseAmount = memberModel.getExpenseAmount() - previousTransactionAmount;
                    newExpenseAmount = newExpenseAmount + updatedTransactionAmount;
                    lastUpdatedMemberIncomeOrExpense = new TblMember(getApplicationContext()).updateMemberSExpenseAmountByMemberId(previousMemberId,
                            newExpenseAmount);
                } else {
                    int newIncomeAmount;
                    newIncomeAmount = memberModel.getIncomeAmount() - previousTransactionAmount;
                    newIncomeAmount = newIncomeAmount + updatedTransactionAmount;
                    lastUpdatedMemberIncomeOrExpense = new TblMember(getApplicationContext()).updateMemberSIncomeAmountByMemberId(previousMemberId,
                            newIncomeAmount);
                }
            } else if (previousTransactionAmount == updatedTransactionAmount && previousIncomeOrExpense != updatedIncomeOrExpense) {
                int newIncomeAmount, newExpenseAmount;

                if (updatedIncomeOrExpense == 1) {

                    newIncomeAmount = memberModel.getIncomeAmount() - updatedTransactionAmount;
                    newExpenseAmount = memberModel.getExpenseAmount() + updatedTransactionAmount;
                    lastUpdatedMemberIncomeOrExpense = new TblMember(getApplicationContext()).updateMemberSIncomeAndExpense(updatedMemberId, newIncomeAmount,
                            newExpenseAmount);
                } else {
                    newExpenseAmount = memberModel.getExpenseAmount() - updatedTransactionAmount;
                    newIncomeAmount = memberModel.getIncomeAmount() + updatedTransactionAmount;
                    lastUpdatedMemberIncomeOrExpense = new TblMember(getApplicationContext()).updateMemberSIncomeAndExpense(updatedMemberId, newIncomeAmount,
                            newExpenseAmount);
                }

            } else if (previousTransactionAmount != updatedTransactionAmount && previousIncomeOrExpense != updatedIncomeOrExpense) {
                int newIncomeAmount, newExpenseAmount;
                if (updatedIncomeOrExpense == 1) {
                    newIncomeAmount = memberModel.getIncomeAmount() - previousTransactionAmount;
                    newExpenseAmount = memberModel.getExpenseAmount() + updatedTransactionAmount;
                    lastUpdatedMemberIncomeOrExpense = new TblMember(getApplicationContext()).updateMemberSIncomeAndExpense(updatedMemberId, newIncomeAmount,
                            newExpenseAmount);

                } else {
                    newExpenseAmount = memberModel.getExpenseAmount() - previousTransactionAmount;
                    newIncomeAmount = memberModel.getIncomeAmount() + updatedTransactionAmount;
                    lastUpdatedMemberIncomeOrExpense = new TblMember(getApplicationContext()).updateMemberSIncomeAndExpense(updatedMemberId, newIncomeAmount,
                            newExpenseAmount);
                }
            }

        } else if (previousMemberId != updatedMemberId) {
            int newIncomeAmount, newExpenseAmount;
            MemberModel previousMemberModel = new TblMember(getApplicationContext()).getMemberModelByMemberId(previousMemberId);
            MemberModel updatedMemberModel = new TblMember(getApplicationContext()).getMemberModelByMemberId(updatedMemberId);
            if (previousTransactionAmount != updatedTransactionAmount && previousIncomeOrExpense == updatedIncomeOrExpense) {
                if (previousIncomeOrExpense == 1) {
                    newExpenseAmount = previousMemberModel.getExpenseAmount() - previousTransactionAmount;
                    long flag = new TblMember(getApplicationContext()).updateMemberSExpenseAmountByMemberId(previousMemberId, newExpenseAmount);
                    if (flag > 0) {
                        newExpenseAmount = updatedMemberModel.getExpenseAmount() + updatedTransactionAmount;
                        lastUpdatedMemberIncomeOrExpense = new TblMember(getApplicationContext()).updateMemberSExpenseAmountByMemberId(updatedMemberId, newExpenseAmount);
                    } else {
                        Toast.makeText(this, R.string.something_went_to_wrong, Toast.LENGTH_SHORT).show();
                    }
                } else {
                    newIncomeAmount = previousMemberModel.getIncomeAmount() - previousTransactionAmount;
                    long flag = new TblMember(getApplicationContext()).updateMemberSIncomeAmountByMemberId(previousMemberId, newIncomeAmount);
                    if (flag > 0) {
                        newIncomeAmount = updatedMemberModel.getIncomeAmount() + updatedTransactionAmount;
                        lastUpdatedMemberIncomeOrExpense = new TblMember(getApplicationContext()).updateMemberSIncomeAmountByMemberId(updatedMemberId, newIncomeAmount);
                    } else {
                        Toast.makeText(this, R.string.something_went_to_wrong, Toast.LENGTH_SHORT).show();
                    }
                }
            } else if (previousTransactionAmount == updatedTransactionAmount && previousIncomeOrExpense != updatedIncomeOrExpense) {
                if (updatedIncomeOrExpense == 1) {
                    newIncomeAmount = previousMemberModel.getIncomeAmount() - previousTransactionAmount;
                    long flag = new TblMember(getApplicationContext()).updateMemberSIncomeAmountByMemberId(previousMemberId, newIncomeAmount);
                    if (flag > 0) {
                        newExpenseAmount = updatedMemberModel.getExpenseAmount() + previousTransactionAmount;
                        lastUpdatedMemberIncomeOrExpense = new TblMember(getApplicationContext()).updateMemberSExpenseAmountByMemberId(updatedMemberId, newExpenseAmount);
                    } else {
                        Toast.makeText(this, R.string.something_went_to_wrong, Toast.LENGTH_SHORT).show();
                    }
                } else {
                    newExpenseAmount = previousMemberModel.getExpenseAmount() - previousTransactionAmount;
                    long flag = new TblMember(getApplicationContext()).updateMemberSExpenseAmountByMemberId(previousMemberId, newExpenseAmount);
                    if (flag > 0) {
                        newIncomeAmount = updatedMemberModel.getExpenseAmount() + updatedTransactionAmount;
                        lastUpdatedMemberIncomeOrExpense = new TblMember(getApplicationContext()).updateMemberSIncomeAmountByMemberId(updatedMemberId, newIncomeAmount);
                    } else {
                        Toast.makeText(this, R.string.something_went_to_wrong, Toast.LENGTH_SHORT).show();
                    }

                }
            } else if (previousTransactionAmount != updatedTransactionAmount && previousIncomeOrExpense != updatedIncomeOrExpense) {
                if (updatedIncomeOrExpense == 1) {
                    newIncomeAmount = previousMemberModel.getIncomeAmount() - previousTransactionAmount;
                    long flag = new TblMember(getApplicationContext()).updateMemberSIncomeAmountByMemberId(previousMemberId, newIncomeAmount);
                    if (flag > 0) {
                        newExpenseAmount = updatedMemberModel.getExpenseAmount() + updatedTransactionAmount;
                        lastUpdatedMemberIncomeOrExpense = new TblMember(getApplicationContext()).updateMemberSExpenseAmountByMemberId(updatedMemberId, newExpenseAmount);
                    } else {
                        Toast.makeText(this, R.string.something_went_to_wrong, Toast.LENGTH_SHORT).show();
                    }
                } else {
                    newExpenseAmount = previousMemberModel.getExpenseAmount() - previousTransactionAmount;
                    long flag = new TblMember(getApplicationContext()).updateMemberSExpenseAmountByMemberId(previousMemberId, newExpenseAmount);
                    if (flag > 0) {
                        newIncomeAmount = updatedMemberModel.getExpenseAmount() + updatedTransactionAmount;
                        lastUpdatedMemberIncomeOrExpense = new TblMember(getApplicationContext()).updateMemberSIncomeAmountByMemberId(updatedMemberId, newIncomeAmount);
                    } else {
                        Toast.makeText(this, R.string.something_went_to_wrong, Toast.LENGTH_SHORT).show();
                    }

                }
            } else if (previousIncomeOrExpense == updatedIncomeOrExpense && previousTransactionAmount == updatedTransactionAmount) {
                if (previousIncomeOrExpense == 1) {
                    newExpenseAmount = previousMemberModel.getExpenseAmount() - previousTransactionAmount;
                    long flag = new TblMember(getApplicationContext()).updateMemberSExpenseAmountByMemberId(previousMemberId, newExpenseAmount);
                    if (flag > 0) {
                        newExpenseAmount = updatedMemberModel.getExpenseAmount() + updatedTransactionAmount;
                        lastUpdatedMemberIncomeOrExpense = new TblMember(getApplicationContext()).updateMemberSExpenseAmountByMemberId(updatedMemberId, newExpenseAmount);
                    } else {
                        Toast.makeText(this, R.string.something_went_to_wrong, Toast.LENGTH_SHORT).show();
                    }
                } else {
                    newIncomeAmount = previousMemberModel.getIncomeAmount() - previousTransactionAmount;
                    long flag = new TblMember(getApplicationContext()).updateMemberSIncomeAmountByMemberId(previousMemberId, newIncomeAmount);
                    if (flag > 0) {
                        newIncomeAmount = updatedMemberModel.getIncomeAmount() + updatedTransactionAmount;
                        lastUpdatedMemberIncomeOrExpense = new TblMember(getApplicationContext()).updateMemberSExpenseAmountByMemberId(updatedMemberId, newIncomeAmount);
                    } else {
                        Toast.makeText(this, R.string.something_went_to_wrong, Toast.LENGTH_SHORT).show();
                    }
                }
            }

        }
        return lastUpdatedMemberIncomeOrExpense;
    }


    //---------------------------------------------------------------\
    //some other utility method like validation ,date converter
    ///-------------------------------------------------------------------\

    boolean isValidEventName() {
        boolean isValid = true;
        if (TextUtils.isEmpty(editTextForTakingValueOfAmountOfTransaction.getText().toString().trim())) {
            isValid = false;
            editTextForTakingValueOfAmountOfTransaction.setError(getString(R.string.error_event_name));
        }
        return isValid;
    }

    void setDataToView() {
        final Calendar calendar = Calendar.getInstance();
        datePicker.setText(
                String.format("%02d", calendar.get(Calendar.DATE)) + "/" +
                        String.format("%02d", ((calendar.get(Calendar.MONTH)) + 1)) + "/" +
                        calendar.get(Calendar.YEAR));
    }
}