package com.ketan.groupexpensemanager.model;

import java.io.Serializable;

public class MemberModel implements Serializable {

    int MemberID;
    String MemberName;
    int GroupID;
    int IncomeAmount;
    int ExpenseAmount;

    public int getIncomeAmount() {
        return IncomeAmount;
    }

    public void setIncomeAmount(int incomeAmount) {
        IncomeAmount = incomeAmount;
    }

    public int getExpenseAmount() {
        return ExpenseAmount;
    }

    public void setExpenseAmount(int expenseAmount) {
        ExpenseAmount = expenseAmount;
    }


    public void setGroupID(int groupID) {
        GroupID = groupID;
    }

    public int getMemberID() {
        return MemberID;
    }

    public void setMemberID(int memberID) {
        MemberID = memberID;
    }

    public String getMemberName() {
        return MemberName;
    }

    public void setMemberName(String memberName) {
        MemberName = memberName;
    }


}
