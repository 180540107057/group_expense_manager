package com.ketan.groupexpensemanager.model;

import java.io.Serializable;

public class TotalAmountModel implements Serializable {

    int TotalIncomeAmount;
    int TotalExpenseAmount;

    public int getTotalIncomeAmount() {
        return TotalIncomeAmount;
    }

    public void setTotalIncomeAmount(int totalIncomeAmount) {
        TotalIncomeAmount = totalIncomeAmount;
    }

    public int getTotalExpenseAmount() {
        return TotalExpenseAmount;
    }

    public void setTotalExpenseAmount(int totalExpenseAmount) {
        TotalExpenseAmount = totalExpenseAmount;
    }
}
