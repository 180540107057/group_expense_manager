package com.ketan.groupexpensemanager.model;

import java.io.Serializable;

public class TransactionModel implements Serializable {

    int TransactionID;
    int TransactionAmount;
    int MemberID;
    int GroupID;
    String MemberName;
    String Date;

    String Discription;

    public String getDiscription() {
        return Discription;
    }

    int isExpense;

    public void setDiscription(String discription) {
        Discription = discription;
    }

    public String getDate() {
        return Date;
    }

    public void setDate(String date) {
        Date = date;
    }

    public int getIsExpense() {
        return isExpense;
    }

    public void setIsExpense(int isExpense) {
        this.isExpense = isExpense;
    }


    public int isExpense() {
        return isExpense;
    }

    public void setExpense(int expense) {
        isExpense = expense;
    }


    public String getMemberName() {
        return MemberName;
    }

    public void setMemberName(String memberName) {
        MemberName = memberName;
    }

    public int getTransactionID() {
        return TransactionID;
    }

    public void setTransactionID(int transactionID) {
        TransactionID = transactionID;
    }

    public int getTransactionAmount() {
        return TransactionAmount;
    }

    public void setTransactionAmount(int transactionAmount) {
        TransactionAmount = transactionAmount;
    }

    public int getMemberID() {
        return MemberID;
    }

    public void setMemberID(int memberID) {
        MemberID = memberID;
    }

    public int getGroupID() {
        return GroupID;
    }

    public void setGroupID(int groupID) {
        GroupID = groupID;
    }


}
