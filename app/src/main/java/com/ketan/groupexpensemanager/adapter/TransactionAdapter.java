package com.ketan.groupexpensemanager.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.ketan.groupexpensemanager.R;
import com.ketan.groupexpensemanager.model.TransactionModel;

import java.util.ArrayList;

public class TransactionAdapter extends RecyclerView.Adapter<TransactionAdapter.ViewHolder> {
    ArrayList<TransactionModel> arrayListOfTransactionDetail;
    Context context;
    InterFaceForOnClickOfTransaction interFaceForOnClickOfTransaction;

    public TransactionAdapter(ArrayList<TransactionModel> arrayListOfTransactionDetail, Context context, InterFaceForOnClickOfTransaction interFaceForOnClickOfTransaction) {
        this.arrayListOfTransactionDetail = arrayListOfTransactionDetail;
        this.context = context;
        this.interFaceForOnClickOfTransaction = interFaceForOnClickOfTransaction;
    }

    @NonNull
    @Override
    public TransactionAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.view_row_for_trasaction_detail, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull TransactionAdapter.ViewHolder holder, int position) {
        TransactionModel transactionModel = arrayListOfTransactionDetail.get(position);
        TextView tvTransactionAmount = holder.textViewForShowTransactionAmount;
        TextView tvMemberName = holder.textViewForShowMemberWhoDidPayment;
        TextView tvIncomeOrExpense = holder.tvIncomeOrExpense;
        TextView tvDate = holder.tvDate;

        tvTransactionAmount.setText(transactionModel.getTransactionAmount() + "");
        tvMemberName.setText(transactionModel.getMemberName() + "");
        tvDate.setText(transactionModel.getDate());

        if (transactionModel.isExpense() == 1) {
            tvTransactionAmount.setTextColor(ContextCompat.getColor(context, R.color.color_red));
            tvMemberName.setTextColor(ContextCompat.getColor(context, R.color.color_red));
            tvIncomeOrExpense.setText(" " + "(Expense)");
            tvDate.setTextColor(ContextCompat.getColor(context, R.color.color_red));
            tvIncomeOrExpense.setTextColor(ContextCompat.getColor(context, R.color.color_red));


        } else {


            tvMemberName.setTextColor(ContextCompat.getColor(context, R.color.color_green));
            tvTransactionAmount.setTextColor(ContextCompat.getColor(context, R.color.color_green));
            tvDate.setTextColor(ContextCompat.getColor(context, R.color.color_green));
            tvIncomeOrExpense.setText(" " + "(Income)");
            tvIncomeOrExpense.setTextColor(ContextCompat.getColor(context, R.color.color_green));

        }


    }

    @Override
    public int getItemCount() {
        return arrayListOfTransactionDetail.size();
    }

    public interface InterFaceForOnClickOfTransaction {
        void onClick(int position);

        void onLongClick(int position);
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {
        TextView textViewForShowMemberWhoDidPayment, textViewForShowTransactionAmount, tvIncomeOrExpense, tvDate;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            textViewForShowMemberWhoDidPayment = itemView.findViewById(R.id.tvForShowMemberWhoDidPayment);
            textViewForShowTransactionAmount = itemView.findViewById(R.id.tvForShowTransactionAmount);
            tvIncomeOrExpense = itemView.findViewById(R.id.tvIncomeOrExpense);
            tvDate = itemView.findViewById(R.id.tvForShowDate);
            itemView.setOnClickListener(this);
            itemView.setOnLongClickListener(this);
        }

        @Override
        public void onClick(View v) {
            interFaceForOnClickOfTransaction.onClick(getAdapterPosition());
        }

        @Override
        public boolean onLongClick(View v) {
            interFaceForOnClickOfTransaction.onLongClick(getAdapterPosition());
            return true;
        }
    }
}
