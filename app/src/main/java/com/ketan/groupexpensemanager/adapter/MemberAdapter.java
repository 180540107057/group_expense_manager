package com.ketan.groupexpensemanager.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ketan.groupexpensemanager.R;
import com.ketan.groupexpensemanager.model.MemberModel;

import java.util.ArrayList;

public class MemberAdapter extends RecyclerView.Adapter<MemberAdapter.ViewHolder> {
    ArrayList<MemberModel> memberList;
    Context context;
    InterfaceForTheClickEventOfTheMemberList interFaceForClickEvent;

    public MemberAdapter(Context context, ArrayList<MemberModel> memberList, InterfaceForTheClickEventOfTheMemberList interFaceForClickEvent) {
        this.memberList = memberList;
        this.context = context;
        this.interFaceForClickEvent = interFaceForClickEvent;

    }

    @NonNull
    @Override
    public MemberAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View viewGroup = inflater.inflate(R.layout.view_raw_member, parent, false);
        ViewHolder viewHolder = new ViewHolder(viewGroup, interFaceForClickEvent);
        return viewHolder;

    }

    @Override
    public void onBindViewHolder(@NonNull MemberAdapter.ViewHolder holder, int position) {
        MemberModel memberModel = memberList.get(position);
        TextView textView = holder.memberName;
        textView.setText(memberModel.getMemberName());

    }

    @Override
    public int getItemCount() {
        return memberList.size();
    }

    public interface InterfaceForTheClickEventOfTheMemberList {

        void onLongClick(int position);

        void onClick(int position);

    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnLongClickListener, View.OnClickListener {
        TextView memberName;
        InterfaceForTheClickEventOfTheMemberList inferFace;

        public ViewHolder(@NonNull View itemView, InterfaceForTheClickEventOfTheMemberList interFaceForClickEvent) {
            super(itemView);
            this.inferFace = interFaceForClickEvent;
            memberName = itemView.findViewById(R.id.text_view_for_member);
            itemView.setOnClickListener(this);
            itemView.setOnLongClickListener(this);


        }


        @Override
        public boolean onLongClick(View v) {
            inferFace.onLongClick(getAdapterPosition());
            return true;
        }

        @Override
        public void onClick(View v) {
            inferFace.onClick(getAdapterPosition());
        }
    }


}
