package com.ketan.groupexpensemanager.adapter;

import android.content.Context;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ketan.groupexpensemanager.R;
import com.ketan.groupexpensemanager.database.TblTotalAmount;
import com.ketan.groupexpensemanager.model.GroupModel;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class GroupAdapter extends RecyclerView.Adapter<GroupAdapter.ViewHolder> {

    ArrayList<GroupModel> groupList;
    Context context;
    OnItemClickListenerForRecyclerView monItemClickListenerForRecyclerView;

    public GroupAdapter(ArrayList<GroupModel> groupList, Context context, OnItemClickListenerForRecyclerView onItemClickListenerForRecyclerView) {
        this.groupList = groupList;
        this.context = context;
        this.monItemClickListenerForRecyclerView = onItemClickListenerForRecyclerView;
    }


    @NonNull
    @Override
    public GroupAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View groupView = inflater.inflate(R.layout.view_group_row, parent, false);
        ViewHolder viewHolder = new ViewHolder(groupView, monItemClickListenerForRecyclerView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull GroupAdapter.ViewHolder holder, int position) {
        CircleImageView imageView = holder.civGroupImage;
        TextView groupName = holder.groupName;
        TextView groupDate = holder.groupDate;
        TextView groupExpenseAmount = holder.groupExpenseAmount;
        TextView groupIncomeAmount = holder.groupIncomeAmount;

        GroupModel groupModel = groupList.get(position);
        int groupId = groupModel.getGroupID();
        groupName.setText(groupModel.getGroupName());
        groupDate.setText(groupModel.getDate());
        groupExpenseAmount.setText(new TblTotalAmount(context).getTotalAmountDetail(groupId).getTotalExpenseAmount() + " ");
        groupIncomeAmount.setText(new TblTotalAmount(context).getTotalAmountDetail(groupId).getTotalIncomeAmount() + " ");

        //check if image not upload than set default image
        String imagePath = groupModel.getImagePath();
        if (imagePath != null) {
            imageView.setImageURI(Uri.parse(imagePath));
        }


    }

    @Override
    public int getItemCount() {
        return groupList.size();
    }

    public interface OnItemClickListenerForRecyclerView {
        void onClick(int position);

        void onLongClick(int position, View view);
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {
        TextView groupName, groupExpenseAmount, groupIncomeAmount, groupDate;
        CircleImageView civGroupImage;
        OnItemClickListenerForRecyclerView onItemClickListenerForRecyclerView;

        public ViewHolder(@NonNull View itemView, OnItemClickListenerForRecyclerView onItemClickListenerForRecyclerView) {
            super(itemView);
            groupName = itemView.findViewById(R.id.tvRow_group_name);
            groupDate = itemView.findViewById(R.id.tvRow_group_date);
            groupExpenseAmount = itemView.findViewById(R.id.tvRow_group_total_expense);
            civGroupImage = itemView.findViewById(R.id.CivUploadGroupImage);
            groupIncomeAmount = itemView.findViewById(R.id.tvRow_group_total_income);
            this.onItemClickListenerForRecyclerView = onItemClickListenerForRecyclerView;
            itemView.setOnClickListener(this);
            itemView.setOnLongClickListener(this);

        }

        @Override
        public void onClick(View v) {
            onItemClickListenerForRecyclerView.onClick(getAdapterPosition());
        }

        @Override
        public boolean onLongClick(View v) {
            onItemClickListenerForRecyclerView.onLongClick(getAdapterPosition(), v);
            return true;
        }
    }


}



