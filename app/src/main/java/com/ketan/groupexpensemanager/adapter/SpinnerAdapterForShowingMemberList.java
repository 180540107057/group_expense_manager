package com.ketan.groupexpensemanager.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.ketan.groupexpensemanager.R;
import com.ketan.groupexpensemanager.model.MemberModel;

import java.util.ArrayList;

public class SpinnerAdapterForShowingMemberList extends BaseAdapter {
    ArrayList<MemberModel> memberListForShowingOnSpinner;
    Context context;

    public SpinnerAdapterForShowingMemberList(ArrayList<MemberModel> memberListForShowingOnSpinner, Context context) {
        this.memberListForShowingOnSpinner = memberListForShowingOnSpinner;
        this.context = context;

    }

    @Override
    public int getCount() {
        return memberListForShowingOnSpinner.size();
    }

    @Override
    public Object getItem(int position) {
        return memberListForShowingOnSpinner.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.view_row_for_spiner_for_showing_member_list, null);
        }
        TextView textView = convertView.findViewById(R.id.text_view_for_spinner_member_list);
        textView.setText(memberListForShowingOnSpinner.get(position).getMemberName());
        return convertView;
    }


}
