package com.ketan.groupexpensemanager.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ketan.groupexpensemanager.R;
import com.ketan.groupexpensemanager.fragment.ReportFragment;
import com.ketan.groupexpensemanager.model.MemberModel;

import java.util.ArrayList;

public class TableRowAdapter extends RecyclerView.Adapter<TableRowAdapter.ViewHolder> {
    Context context;
    ArrayList<MemberModel> memberList;

    public TableRowAdapter(Context context, ArrayList<MemberModel> memberList) {
        this.context = context;
        this.memberList = memberList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.view_row_report_table, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        MemberModel memberModel = this.memberList.get(position);
        int expenseAmount = memberModel.getExpenseAmount();
        int incomeAmount = memberModel.getIncomeAmount();
        int expensePerHead = new ReportFragment().returnExpensePerHead();
        holder.member.setText(memberModel.getMemberName());
        holder.income.setText(incomeAmount + " ");
        holder.expense.setText(expenseAmount + "");
        int refundedAmount = 0;
        int ownsAmount = 0;
        int memberSize = memberList.size();
        if (memberSize > 1) {
            if (expenseAmount >= expensePerHead) {
                refundedAmount = expenseAmount - expensePerHead;
            } else {

                ownsAmount = expensePerHead - expenseAmount;
            }
        }
        holder.refund.setText(refundedAmount + " ");
        holder.owns.setText(ownsAmount + " ");


    }

    @Override
    public int getItemCount() {
        return memberList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView member, income, expense, refund, owns;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            income = itemView.findViewById(R.id.tvIncomeColumn);
            member = itemView.findViewById(R.id.tvMemberColumn);
            expense = itemView.findViewById(R.id.tvExpenseColumn);
            refund = itemView.findViewById(R.id.tvRefundColumn);
            owns = itemView.findViewById(R.id.tvOwensColumn);
        }
    }
}
