package com.ketan.groupexpensemanager.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.ketan.groupexpensemanager.model.MemberModel;

import java.util.ArrayList;

public class TblMember extends MyDataBase {
    public static final String TABLE_NAME_2 = "TblMember";
    public static final String MEMBER_ID = "MemberID";
    public static final String MEMBER_NAME = "MemberName";
    public static final String GROUP_ID = "GroupID";
    public static final String INCOME_AMOUNT = "IncomeAmount";
    public static final String EXPENSE_AMOUNT = "ExpenseAmount";

    public TblMember(Context context) {
        super(context);
    }

    public long insertGroup(String memberName, int GroupID) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(MEMBER_NAME, memberName);
        cv.put(GROUP_ID, GroupID);
        long lastInsertedId = db.insert(TABLE_NAME_2, null, cv);
        db.close();
        return lastInsertedId;
    }

    public long insertAmountToMemberAccount(int incomeAmount, int expenseAmount, int memberID) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(INCOME_AMOUNT, incomeAmount);
        cv.put(EXPENSE_AMOUNT, expenseAmount);
        long AmountAdded = db.update(TABLE_NAME_2, cv, MEMBER_ID + "=?", new String[]{String.valueOf(memberID)});
        db.close();
        return AmountAdded;

    }

    public long deleteGroupByIdInTblMember(int groupId) {
        SQLiteDatabase db = getWritableDatabase();
        long i = db.delete(TABLE_NAME_2, GROUP_ID + "=?", new String[]{String.valueOf(groupId)});
        db.close();
        return i;

    }

    public ArrayList<MemberModel> getMemberByID(int groupId) {

        SQLiteDatabase db = getReadableDatabase();
        ArrayList<MemberModel> memberList = new ArrayList<>();
        String query = "SELECT * FROM TblMember inner join TblGroup on TblGroup.GroupID = TblMember.GroupID WHERE TblGroup.GroupID = ?";
        Cursor cursor = db.rawQuery(query, new String[]{String.valueOf(groupId)});
        cursor.moveToFirst();
        for (int i = 0; i < cursor.getCount(); i++) {

            MemberModel memberModel = new MemberModel();
            memberModel.setMemberName(cursor.getString(cursor.getColumnIndex(MEMBER_NAME)));
            memberModel.setMemberID(cursor.getInt(cursor.getColumnIndex(MEMBER_ID)));
            memberModel.setGroupID(cursor.getInt(cursor.getColumnIndex(GROUP_ID)));
            memberModel.setExpenseAmount(cursor.getInt(cursor.getColumnIndex(EXPENSE_AMOUNT)));
            memberModel.setIncomeAmount(cursor.getInt(cursor.getColumnIndex(INCOME_AMOUNT)));
            memberList.add(memberModel);
            cursor.moveToNext();
        }

        cursor.close();
        db.close();
        return memberList;
    }

    public int getTotalNumberOfMemberByGroupId(int groupID) {
        SQLiteDatabase db = getReadableDatabase();
        String query = "SELECT MemberID FROM TblMember Inner join TblGroup on TblMember.GroupID = TblGroup.GroupID where TblMember.GroupID = ?";
        Cursor cursor = db.rawQuery(query, new String[]{String.valueOf(groupID)});
        cursor.moveToFirst();
        int count = 0;
        for (int i = 0; i < cursor.getCount(); i++) {
            count++;
            cursor.moveToNext();
        }
        db.close();
        cursor.close();
        return count;
    }

    public long updateUserById(int memberID, String memberName) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(MEMBER_NAME, memberName);
        long lastUpdatedMember = db.update(TABLE_NAME_2, cv, MEMBER_ID + "=?", new String[]{String.valueOf(memberID)});
        db.close();
        return lastUpdatedMember;
    }

    public long deleteMemberByMemberId(int memberID) {
        SQLiteDatabase db = getWritableDatabase();
        long lastDeletedMember = db.delete(TABLE_NAME_2, MEMBER_ID + "=?", new String[]{String.valueOf(memberID)});
        db.close();
        return lastDeletedMember;
    }


    public MemberModel getMemberModelByMemberId(int memberId) {
        SQLiteDatabase db = getReadableDatabase();
        MemberModel memberModel = new MemberModel();
        String query = " SELECT * FROM TblMember where MemberID =?";
        Cursor cursor = db.rawQuery(query, new String[]{String.valueOf(memberId)});
        cursor.moveToFirst();
        for (int i = 0; i < cursor.getCount(); i++) {
            memberModel.setMemberName(cursor.getString(cursor.getColumnIndex(MEMBER_NAME)));
            memberModel.setExpenseAmount(cursor.getInt(cursor.getColumnIndex(EXPENSE_AMOUNT)));
            memberModel.setIncomeAmount(cursor.getInt(cursor.getColumnIndex(INCOME_AMOUNT)));
            cursor.moveToNext();
        }
        cursor.close();
        db.close();
        return memberModel;
    }

    public long updateMemberSIncomeAmountByMemberId(int memberId, int updatedIncomeAmount) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(INCOME_AMOUNT, updatedIncomeAmount);
        long lastUpdatedMemberIncome = db.update(TABLE_NAME_2, cv, MEMBER_ID + "=?", new String[]{String.valueOf(memberId)});
        db.close();
        return lastUpdatedMemberIncome;
    }

    public long updateMemberSExpenseAmountByMemberId(int memberId, int updatedExpenseAmount) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(EXPENSE_AMOUNT, updatedExpenseAmount);
        long lastUpdatedMemberExpense = db.update(TABLE_NAME_2, cv, MEMBER_ID + "=?", new String[]{String.valueOf(memberId)});
        db.close();
        return lastUpdatedMemberExpense;
    }

    public long updateMemberSIncomeAndExpense(int memberId, int updatedIncome, int updatedExpense) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(INCOME_AMOUNT, updatedIncome);
        cv.put(EXPENSE_AMOUNT, updatedExpense);
        long lastUpdatedMemberSIncomeAndExpense = db.update(TABLE_NAME_2, cv, MEMBER_ID + "=?", new String[]{String.valueOf(memberId)});
        db.close();
        return lastUpdatedMemberSIncomeAndExpense;
    }


}
