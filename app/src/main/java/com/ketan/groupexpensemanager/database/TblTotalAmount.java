package com.ketan.groupexpensemanager.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.ketan.groupexpensemanager.model.TotalAmountModel;

public class TblTotalAmount extends MyDataBase {

    public static final String TABLE_NAME_4 = "TblTotalAmount";
    public static final String TOTAL_INCOME_AMOUNT = "TotalIncomeAmount";
    public static final String TOTAL_EXPENSE_AMOUNT = "TotalExpenseAmount";
    public static final String GROUP_ID = "GroupID";

    public TblTotalAmount(Context context) {
        super(context);
    }

    public long insertTotalAmount(int totalIncomeAmount, int totalExpenseAmount, int GroupID) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(TOTAL_INCOME_AMOUNT, totalIncomeAmount);
        cv.put(TOTAL_EXPENSE_AMOUNT, totalExpenseAmount);
        long k = db.update(TABLE_NAME_4, cv, GROUP_ID + "=?", new String[]{String.valueOf(GroupID)});
        db.close();
        return k;
    }

    public long insertedGroupID() {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(TOTAL_EXPENSE_AMOUNT, 0);
        cv.put(TOTAL_INCOME_AMOUNT, 0);
        long lastInsertedGroup_ID = db.insert(TABLE_NAME_4, null, cv);
        db.close();
        return lastInsertedGroup_ID;
    }

    public TotalAmountModel getTotalAmountDetail(int groupID) {
        SQLiteDatabase db = getReadableDatabase();
        TotalAmountModel totalAmountModel = new TotalAmountModel();
        String query = "SELECT * FROM TblTotalAmount  inner join TblGroup on TblTotalAmount.GroupID = TblGroup.GroupID  where TblTotalAmount.GroupID= ?";
        Cursor cursor = db.rawQuery(query, new String[]{String.valueOf(groupID)});
        cursor.moveToFirst();
        for (int i = 0; i < cursor.getCount(); i++) {
            totalAmountModel.setTotalExpenseAmount(cursor.getInt(cursor.getColumnIndex(TOTAL_EXPENSE_AMOUNT)));
            totalAmountModel.setTotalIncomeAmount(cursor.getInt(cursor.getColumnIndex(TOTAL_INCOME_AMOUNT)));
            cursor.moveToNext();
        }
        db.close();
        cursor.close();
        return totalAmountModel;
    }

    public long deleteGroupByIdInTblTotalAmount(int group_id) {
        SQLiteDatabase db = getWritableDatabase();
        long i = db.delete(TABLE_NAME_4, GROUP_ID + "=?", new String[]{String.valueOf(group_id)});
        db.close();
        return i;
    }

    public long updateTotalTransactionDetail(int groupId, int incomeAmount, int expenseAmount) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(TOTAL_INCOME_AMOUNT, incomeAmount);
        cv.put(TOTAL_EXPENSE_AMOUNT, expenseAmount);

        long lastUpdatedTotalTransaction = db.update(TABLE_NAME_4, cv, GROUP_ID + "=?", new String[]{String.valueOf(groupId)});
        db.close();
        return lastUpdatedTotalTransaction;
    }

    public long updateTransactionAmountWhichIsDeleted(int groupId, int incomeAmount, int expenseAmount) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(TOTAL_INCOME_AMOUNT, incomeAmount);
        cv.put(TOTAL_EXPENSE_AMOUNT, expenseAmount);
        long i = db.update(TABLE_NAME_4, cv, GROUP_ID + "=?", new String[]{String.valueOf(groupId)});
        db.close();
        return i;
    }

}
