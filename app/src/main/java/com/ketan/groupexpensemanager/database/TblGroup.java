package com.ketan.groupexpensemanager.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.ketan.groupexpensemanager.model.GroupModel;

import java.util.ArrayList;

public class TblGroup extends MyDataBase {

    public static final String TABLE_NAME = "TblGroup";
    public static final String GROUP_ID = "GroupID";
    public static final String GROUP_NAME = "GroupName";
    public static final String CREATED_DATE = "CreatedDate";
    public static final String IMAGE_PATH = "ImagePath";


    public TblGroup(Context context) {
        super(context);
    }

    public long insertGroup(String name, String date, String imagePath) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(GROUP_NAME, name);
        cv.put(CREATED_DATE, date);
        cv.put(IMAGE_PATH, imagePath);
        long lastInsertedId = db.insert(TABLE_NAME, null, cv);
        db.close();
        return lastInsertedId;
    }

    public long deleteGroupByIdInTblGroup(int group_id) {
        SQLiteDatabase db = getWritableDatabase();
        long i = db.delete(TABLE_NAME, GROUP_ID + "=?", new String[]{String.valueOf(group_id)});
        db.close();
        return i;
    }

    public ArrayList<GroupModel> getGroupDetail() {
        SQLiteDatabase db = getReadableDatabase();
        ArrayList<GroupModel> groupDetail = new ArrayList<>();
        String query = " SELECT * FROM " + TABLE_NAME;
        Cursor cursor = db.rawQuery(query, null);
        cursor.moveToFirst();
        for (int i = 0; i < cursor.getCount(); i++) {
            GroupModel groupModel = new GroupModel();
            groupModel.setGroupID(cursor.getInt(cursor.getColumnIndex(GROUP_ID)));
            groupModel.setGroupName(cursor.getString(cursor.getColumnIndex(GROUP_NAME)));
            groupModel.setDate(cursor.getString(cursor.getColumnIndex(CREATED_DATE)));
            groupModel.setImagePath(cursor.getString(cursor.getColumnIndex(IMAGE_PATH)));
            groupDetail.add(groupModel);
            cursor.moveToNext();
        }
        cursor.close();
        db.close();
        return groupDetail;
    }

    public long updateGroupByID(int group_id, String group_name, String group_date, String imagePath) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(GROUP_NAME, group_name);
        cv.put(CREATED_DATE, group_date);
        cv.put(IMAGE_PATH, imagePath);
        long lastUpdatedGroup = db.update(TABLE_NAME, cv, GROUP_ID + "=?", new String[]{String.valueOf(group_id)});
        db.close();
        return lastUpdatedGroup;
    }

}
