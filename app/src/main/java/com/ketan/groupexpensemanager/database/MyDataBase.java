package com.ketan.groupexpensemanager.database;

import android.content.Context;

import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;

public class MyDataBase extends SQLiteAssetHelper {

    public static final String DATABASE_NAME = "GroupExpenseManager.db";
    public static final int DATABASE_VERSION = 1;

    public MyDataBase(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }
}
