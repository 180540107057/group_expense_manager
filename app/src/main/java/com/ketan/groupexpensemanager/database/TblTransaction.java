package com.ketan.groupexpensemanager.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.ketan.groupexpensemanager.model.TransactionModel;

import java.util.ArrayList;

public class TblTransaction extends MyDataBase {

    public static final String TABLE_NAME_3 = "TblTransaction";
    public static final String TRANSACTION_ID = "TransactionID";
    public static final String TRANSACTION_AMOUNT = "TransactionAmount";
    public static final String MEMBER_ID = "MemberID";
    public static final String GROUP_ID = "GroupID";
    public static final String MEMBER_NAME = "MemberName";
    public static final String IS_EXPENSE = "IsExpense";
    public static final String TRANSACTION_DATE = "Date";
    public static final String TRANSACTION_DISCRIPTION = "Discription";


    public TblTransaction(Context context) {
        super(context);
    }

    public long insertTransactionDetail(int transactionAmount,
                                        int groupID,
                                        int memberID,
                                        String memberName,
                                        int isExpense,
                                        String date,
                                        String description) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(TRANSACTION_AMOUNT, transactionAmount);
        cv.put(MEMBER_ID, memberID);
        cv.put(GROUP_ID, groupID);
        cv.put(MEMBER_NAME, memberName);
        cv.put(IS_EXPENSE, isExpense);
        cv.put(TRANSACTION_DATE, date);
        cv.put(TRANSACTION_DISCRIPTION, description);
        long lastInsertedId = db.insert(TABLE_NAME_3, null, cv);
        db.close();
        return lastInsertedId;
    }

    public ArrayList<TransactionModel> getTransactionDetail(int groupID) {
        SQLiteDatabase db = getReadableDatabase();
        ArrayList<TransactionModel> transactionDetail = new ArrayList<>();
        String query = "SELECT * FROM TblTransaction INNER JOIN TblGroup ON TblTransaction.GroupID = TblGroup.GroupID WHERE TblTransaction.GroupID = ?";
        Cursor cursor = db.rawQuery(query, new String[]{String.valueOf(groupID)});
        cursor.moveToFirst();
        for (int i = 0; i < cursor.getCount(); i++) {
            TransactionModel transactionModel = new TransactionModel();
            transactionModel.setTransactionID(cursor.getInt(cursor.getColumnIndex(TRANSACTION_ID)));
            transactionModel.setGroupID(cursor.getInt(cursor.getColumnIndex(GROUP_ID)));
            transactionModel.setTransactionAmount(cursor.getInt(cursor.getColumnIndex(TRANSACTION_AMOUNT)));
            transactionModel.setMemberID(cursor.getInt(cursor.getColumnIndex(MEMBER_ID)));
            transactionModel.setMemberName(cursor.getString(cursor.getColumnIndex(MEMBER_NAME)));
            transactionModel.setExpense(cursor.getInt(cursor.getColumnIndex(IS_EXPENSE)));
            transactionModel.setDate(cursor.getString(cursor.getColumnIndex(TRANSACTION_DATE)));
            transactionModel.setDiscription(cursor.getString(cursor.getColumnIndex(TRANSACTION_DISCRIPTION)));
            transactionDetail.add(transactionModel);
            cursor.moveToNext();
        }
        cursor.close();
        db.close();
        return transactionDetail;
    }

    public long deleteGroupByIdInTblTransaction(int group_id) {
        SQLiteDatabase db = getWritableDatabase();
        long i = db.delete(TABLE_NAME_3, GROUP_ID + "=?", new String[]{String.valueOf(group_id)});
        db.close();
        return i;
    }

    public boolean isMemberAssociatedWithTransaction(int member_id) {
        SQLiteDatabase db = getReadableDatabase();
        String query = "SELECT MemberID FROM TblTransaction";
        Cursor cursor = db.rawQuery(query, null);
        cursor.moveToFirst();
        boolean isAssociatedWithTransaction = false;
        for (int i = 0; i < cursor.getCount(); i++) {
            if (member_id == cursor.getInt(cursor.getColumnIndex(MEMBER_ID))) {
                isAssociatedWithTransaction = true;
                break;
            }
            cursor.moveToNext();
        }
        db.close();
        cursor.close();
        return isAssociatedWithTransaction;
    }

    public long updateMemberNameByMemberId(int memberId, String updatedMemberName) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(MEMBER_NAME, updatedMemberName);
        long lastUpdatedMember = db.update(TABLE_NAME_3, cv, MEMBER_ID + "=?", new String[]{String.valueOf(memberId)});
        db.close();
        return lastUpdatedMember;
    }

    public long updateTransactionByTransactionId(int transactionId, int memberId, String memberName, int isExpense, int transactionAmount, String updatedDate, String description) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(MEMBER_ID, memberId);
        cv.put(MEMBER_NAME, memberName);
        cv.put(IS_EXPENSE, isExpense);
        cv.put(TRANSACTION_AMOUNT, transactionAmount);
        cv.put(TRANSACTION_DATE, updatedDate);
        cv.put(TRANSACTION_DISCRIPTION, description);
        long lastUpdatedTransaction = db.update(TABLE_NAME_3, cv, TRANSACTION_ID + "=?", new String[]{String.valueOf(transactionId)});
        db.close();
        return lastUpdatedTransaction;
    }

    public long deleteTransactionByTransactionId(int transactionID) {
        SQLiteDatabase db = getWritableDatabase();
        long lastDeletedTransaction = db.delete(TABLE_NAME_3, TRANSACTION_ID + "=?", new String[]{String.valueOf(transactionID)});
        db.close();
        return lastDeletedTransaction;
    }

    public long updateTransactionDateByTransactionId(int transactionID, String date) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(TRANSACTION_DATE, date);
        long lastUpdatedDate = db.update(TABLE_NAME_3, cv, TRANSACTION_ID + "=?", new String[]{String.valueOf(transactionID)});
        db.close();
        return lastUpdatedDate;
    }


}
