package com.ketan.groupexpensemanager.fragment;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.ketan.groupexpensemanager.R;
import com.ketan.groupexpensemanager.activity.ActivityAddTransaction;
import com.ketan.groupexpensemanager.activity.ActivityMain;
import com.ketan.groupexpensemanager.adapter.TransactionAdapter;
import com.ketan.groupexpensemanager.database.TblMember;
import com.ketan.groupexpensemanager.database.TblTotalAmount;
import com.ketan.groupexpensemanager.database.TblTransaction;
import com.ketan.groupexpensemanager.model.MemberModel;
import com.ketan.groupexpensemanager.model.TotalAmountModel;
import com.ketan.groupexpensemanager.model.TransactionModel;
import com.ketan.groupexpensemanager.util.Constant;

import java.util.ArrayList;

public class TransactionFragment extends Fragment implements TransactionAdapter.InterFaceForOnClickOfTransaction {
    static int groupID;
    ArrayList<TransactionModel> arrayListOfTransactionDetail;
    RecyclerView rcvForTransactionList;
    FloatingActionButton fabForAddTransaction;
    TransactionAdapter transactionAdapter;
    TextView tvForShowTotalMemberInTf, tvForShowTotalIncomeInTf, tvForShowTotalExpenseInTf, tvNoTransactionFound;
    MyBroadCastReceiverForUpdateMemberName myBroadCastReceiverForUpdateMemberName = new MyBroadCastReceiverForUpdateMemberName();

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragement_transaction, container, false);
        initFindId(view);
        this.groupID = new ActivityMain().getGroupId();
        clickEventOfFloatingButtonForAddTransaction();
        setAdapterShowingTransactionDetail(groupID);
        showBasicInfoOfEvent(groupID);
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(myBroadCastReceiverForUpdateMemberName, new IntentFilter(Constant.BROADCAST_FOR_UPDATE_MEMBER_DETAIL_OF_TABLE));
        return view;
    }

    public void initFindId(View view) {
        rcvForTransactionList = view.findViewById(R.id.rcv_for_transaction_list);
        fabForAddTransaction = view.findViewById(R.id.fabForAddTransaction);
        tvForShowTotalMemberInTf = view.findViewById(R.id.tvForShowTotalMemberInTF);
        tvForShowTotalIncomeInTf = view.findViewById(R.id.tvForShowTotalIncomeInTF);
        tvForShowTotalExpenseInTf = view.findViewById(R.id.tvForShowTotalExpenseInTF);
        tvNoTransactionFound = view.findViewById(R.id.tvNoTransactionFound);
    }


    public void setAdapterShowingTransactionDetail(int group_ID) {
        arrayListOfTransactionDetail = new ArrayList<>();
        arrayListOfTransactionDetail.addAll(new TblTransaction(getActivity()).getTransactionDetail(group_ID));
        transactionNotFound();
        transactionAdapter = new TransactionAdapter(arrayListOfTransactionDetail, getActivity(), this);
        rcvForTransactionList.setAdapter(transactionAdapter);
        rcvForTransactionList.setLayoutManager(new LinearLayoutManager(getActivity()));

    }

    // at bottom of activity it will show total income, expense, member
    private void showBasicInfoOfEvent(int group_ID) {
        tvForShowTotalExpenseInTf.setText(" " + new TblTotalAmount(getActivity()).getTotalAmountDetail(group_ID).getTotalExpenseAmount());
        tvForShowTotalIncomeInTf.setText(" " + new TblTotalAmount(getActivity()).getTotalAmountDetail(group_ID).getTotalIncomeAmount());
        tvForShowTotalMemberInTf.setText(" " + new TblMember(getActivity()).getTotalNumberOfMemberByGroupId(group_ID));
    }

    private void transactionNotFound() {
        if (!(arrayListOfTransactionDetail.size() > 0)) {
            rcvForTransactionList.setVisibility(View.GONE);
            tvNoTransactionFound.setVisibility(View.VISIBLE);
        }
    }


    public void clickEventOfFloatingButtonForAddTransaction() {
        fabForAddTransaction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int numberOfMemberInGroup = new TblMember(getActivity()).getTotalNumberOfMemberByGroupId(groupID);
                if (numberOfMemberInGroup > 0) {
                    Intent intent = new Intent(getActivity(), ActivityAddTransaction.class);
                    startActivity(intent);
                } else {

                    Toast.makeText(getActivity(), R.string.error_no_member_in_this_group, Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    //click event of transction for update
    @Override
    public void onClick(int position) {
        Intent intent = new Intent(getActivity(), ActivityAddTransaction.class);
        TransactionModel transactionModel = arrayListOfTransactionDetail.get(position);
        intent.putExtra(Constant.UPDATE_TRANSACTION_INTENT_NAME, transactionModel);
        startActivity(intent);
    }

    // on longclick it will delete transaction
    @Override
    public void onLongClick(int position) {

        askOptionForDeleteTransaction(position).show();

    }

    private AlertDialog askOptionForDeleteTransaction(int position) {
        final int positionTemp = position;
        AlertDialog deleteMemberDialogBox;
        deleteMemberDialogBox = new AlertDialog.Builder(getActivity())
                // set message, title, and icon
                .setTitle(R.string.delete_transtion)
                .setMessage(R.string.confirmation_delete_transaction)


                .setPositiveButton(R.string.delete, new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int whichButton) {
                        deleteTransactionById(positionTemp);
                        dialog.dismiss();
                    }

                })
                .setNegativeButton(R.string.cancle, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        dialog.dismiss();

                    }
                })
                .create();
        return deleteMemberDialogBox;
    }

    // logic of deleting transaction
    protected void deleteTransactionById(int position) {
        long k = 0;
        TransactionModel transactionModel = arrayListOfTransactionDetail.get(position);
        int memberId = transactionModel.getMemberID();
        MemberModel memberModel = new TblMember(getActivity()).getMemberModelByMemberId(memberId);
        int totalIncomeForMember = memberModel.getIncomeAmount();
        int totalExpenseForMember = memberModel.getExpenseAmount();
        TotalAmountModel totalAmountModel = new TblTotalAmount(getActivity()).getTotalAmountDetail(groupID);
        int totalIncomeAmount = totalAmountModel.getTotalIncomeAmount();
        int totalExpenseAmount = totalAmountModel.getTotalExpenseAmount();
        int deletedAmount = transactionModel.getTransactionAmount();
        long i = new TblTransaction(getActivity()).deleteTransactionByTransactionId(transactionModel.getTransactionID());
        if (transactionModel.isExpense() == 1) {
            totalExpenseAmount = totalExpenseAmount - deletedAmount;
            totalExpenseForMember = totalExpenseForMember - deletedAmount;
            k = new TblMember(getActivity()).updateMemberSExpenseAmountByMemberId(memberId, totalExpenseForMember);
        } else {
            totalIncomeAmount = totalIncomeAmount - deletedAmount;
            totalIncomeForMember = totalIncomeForMember - deletedAmount;
            k = new TblMember(getActivity()).updateMemberSIncomeAmountByMemberId(memberId, totalIncomeForMember);
        }
        long j = new TblTotalAmount(getActivity()).updateTransactionAmountWhichIsDeleted(transactionModel.getGroupID(), totalIncomeAmount, totalExpenseAmount);
        if (i > 0 && j > 0 && k > 0) {

            arrayListOfTransactionDetail.remove(position);
            transactionAdapter.notifyDataSetChanged();
            transactionNotFound();
            Toast.makeText(getActivity(), "Transaction Deleted SuccessFully", Toast.LENGTH_SHORT).show();
            LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(new Intent(Constant.BROADCAST_FOR_UPDATE_MEMBER_DETAIL_OF_TABLE));


        } else {
            Toast.makeText(getActivity(), "Something Went Wrong", Toast.LENGTH_SHORT).show();
        }


    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(myBroadCastReceiverForUpdateMemberName);
    }

    private class MyBroadCastReceiverForUpdateMemberName extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            setAdapterShowingTransactionDetail(groupID);
            showBasicInfoOfEvent(groupID);
        }
    }
}