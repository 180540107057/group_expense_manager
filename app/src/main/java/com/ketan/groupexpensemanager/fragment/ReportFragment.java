package com.ketan.groupexpensemanager.fragment;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ketan.groupexpensemanager.R;
import com.ketan.groupexpensemanager.activity.ActivityMain;
import com.ketan.groupexpensemanager.adapter.TableRowAdapter;
import com.ketan.groupexpensemanager.database.TblMember;
import com.ketan.groupexpensemanager.database.TblTotalAmount;
import com.ketan.groupexpensemanager.util.Constant;


public class ReportFragment extends Fragment {
    static int expensePerHeads = 0;
    TextView tvForTotalMember, tvForTotalIncome, tvForTotalExpense, tvForExpensePerHead;
    RecyclerView recyclerViewForTable;
    TableRowAdapter tableRowAdapter;
    MyBroadCastReceiverForMemberChange myBroadCastReceiverForMemberChange = new MyBroadCastReceiverForMemberChange();


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = LayoutInflater.from(getActivity()).inflate(R.layout.fragment_report, container, false);
        initFindID(v);
        setTextForView();
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(myBroadCastReceiverForMemberChange, new IntentFilter(Constant.BROADCAST_FOR_UPDATE_MEMBER_DETAIL_OF_TABLE));
        setAdapterForTableRow();
        return v;
    }


    protected void initFindID(View v) {
        tvForTotalMember = v.findViewById(R.id.etForTotalMember);
        tvForTotalIncome = v.findViewById(R.id.etForTotalIncome);
        tvForTotalExpense = v.findViewById(R.id.etForTotalExpense);
        tvForExpensePerHead = v.findViewById(R.id.etForPerHead);
        recyclerViewForTable = v.findViewById(R.id.rcvTableForReport);

    }

    void setAdapterForTableRow() {
        tableRowAdapter = new TableRowAdapter(getActivity(), new TblMember(getActivity()).getMemberByID(new ActivityMain().getGroupId()));
        recyclerViewForTable.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerViewForTable.setAdapter(tableRowAdapter);
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(myBroadCastReceiverForMemberChange);
    }

    private void setTextForView() {
        int groupid = new ActivityMain().getGroupId();
        int memberNumber = new TblMember(getActivity()).getTotalNumberOfMemberByGroupId(groupid);
        int totalIncome = new TblTotalAmount(getActivity()).getTotalAmountDetail(groupid).getTotalIncomeAmount();
        int totalExpense = new TblTotalAmount(getActivity()).getTotalAmountDetail(groupid).getTotalExpenseAmount();

        if (memberNumber > 0) {
            this.expensePerHeads = totalExpense / memberNumber;
            expensePerHeads = totalExpense / memberNumber;
        }

        tvForTotalMember.setText(memberNumber + "");
        tvForTotalIncome.setText(totalIncome + "");
        tvForTotalExpense.setText(totalExpense + "");
        tvForExpensePerHead.setText(expensePerHeads + "");

    }

    public int returnExpensePerHead() {
        return this.expensePerHeads;
    }

    private class MyBroadCastReceiverForMemberChange extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            setAdapterForTableRow();
            setTextForView();
        }
    }

}
