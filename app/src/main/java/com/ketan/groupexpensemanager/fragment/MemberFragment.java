package com.ketan.groupexpensemanager.fragment;

import android.Manifest;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.ketan.groupexpensemanager.R;
import com.ketan.groupexpensemanager.activity.ActivityMain;
import com.ketan.groupexpensemanager.adapter.MemberAdapter;
import com.ketan.groupexpensemanager.database.TblMember;
import com.ketan.groupexpensemanager.database.TblTransaction;
import com.ketan.groupexpensemanager.model.MemberModel;
import com.ketan.groupexpensemanager.util.Constant;

import java.util.ArrayList;

import soup.neumorphism.NeumorphImageView;
import soup.neumorphism.NeumorphTextView;

public class MemberFragment extends Fragment implements MemberAdapter.InterfaceForTheClickEventOfTheMemberList {
    static int groupId;
    ArrayList<MemberModel> memberList = new ArrayList<>();
    MemberAdapter memberAdapter;
    static final int REQUEST_SELECT_CONTACT = 1;
    RecyclerView rcv_for_member_list;
    FloatingActionButton fabForAddMember;
    //Object For  PopUpMenu
    EditText etForMemberName;
    Button btnMemberSubmit;
    TextView tvNoMemberFound, tvShowTotalMember, tvShowEventName;
    NeumorphTextView tvForShowTitle;
    NeumorphImageView IvContact;

    @Override
    public View onCreateView(@NonNull final LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_member, container, false);
        this.groupId = new ActivityMain().getGroupId();
        initFindId(view);
        setAdapterForShowingMemberList(groupId);
        showNoMemberFound();
        clickEventOFFloatingButtonForAddingMember();
        showBasicInfoOFMember(groupId);
        return view;
    }


    public void initFindId(View view) {
        fabForAddMember = view.findViewById(R.id.fabForAddMember);
        rcv_for_member_list = view.findViewById(R.id.rcv_for_member_list);
        tvNoMemberFound = view.findViewById(R.id.tvNoMemberFound);
        tvShowEventName = view.findViewById(R.id.tvForShowEventNameInMF);
        tvShowTotalMember = view.findViewById(R.id.tvForShowTotalMemberInMF);

    }

    public void clickEventOFFloatingButtonForAddingMember() {
        fabForAddMember.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showCustomDialog();
            }
        });

    }


    public void setAdapterForShowingMemberList(int group_id) {

        memberList.addAll(new TblMember(getActivity()).getMemberByID(group_id));
        showNoMemberFound();
        memberAdapter = new MemberAdapter(getActivity(), memberList, this);
        rcv_for_member_list.setAdapter(memberAdapter);
        rcv_for_member_list.setLayoutManager(new LinearLayoutManager(getActivity()));

    }

    // onLongClick delete member
    @Override
    public void onLongClick(int position) {

        askOptionForDeleteMember(position).show();
    }


    // onclick for update member
    @Override
    public void onClick(int position) {
        showCustomDialog(position);
    }

    protected void deleteMemberByID(int memberID, int position) {
        long lastDeletedMember = new TblMember(getActivity()).deleteMemberByMemberId(memberID);
        if (lastDeletedMember > 0) {
            memberList.remove(position);
            memberAdapter.notifyDataSetChanged();
            Toast.makeText(getActivity(), R.string.member_deleted_sucessfully, Toast.LENGTH_SHORT).show();
            showBasicInfoOFMember(groupId);
            showNoMemberFound();
            LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(new Intent(Constant.BROADCAST_FOR_UPDATE_MEMBER_DETAIL_OF_TABLE));

        } else {
            Toast.makeText(getActivity(), R.string.something_went_to_wrong, Toast.LENGTH_LONG).show();
        }

    }

    // is  there is no member then show textView member no member found
    private void showNoMemberFound() {
        if (memberList.size() <= 0) {
            tvNoMemberFound.setVisibility(View.VISIBLE);
            rcv_for_member_list.setVisibility(View.GONE);
        } else {
            tvNoMemberFound.setVisibility(View.GONE);
            rcv_for_member_list.setVisibility(View.VISIBLE);
        }
    }

    // show at bottom for basic information like numbers of member , group name.;
    private void showBasicInfoOFMember(int groupId) {
        tvShowEventName.setText(new ActivityMain().getGroupName() + " ");
        tvShowTotalMember.setText(new TblMember(getActivity()).getTotalNumberOfMemberByGroupId(groupId) + " ");
    }

    // open dialog box when user need to delete member
    private AlertDialog askOptionForDeleteMember(int position) {
        final int positionTemp = position;
        AlertDialog deleteMemberDialogBox;
        deleteMemberDialogBox = new AlertDialog.Builder(getActivity())
                // set message, title, and icon
                .setTitle(R.string.delete_member)
                .setMessage(R.string.confirm_delete_member)


                .setPositiveButton(R.string.delete, new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int whichButton) {
                        int memberId = memberList.get(positionTemp).getMemberID();
                        boolean is = new TblTransaction(getActivity()).isMemberAssociatedWithTransaction(memberId);
                        if (!is) {
                            deleteMemberByID(memberId, positionTemp);
                        } else {
                            Toast.makeText(getActivity(), R.string.error_first_remove_transaction, Toast.LENGTH_SHORT).show();
                        }
                        dialog.dismiss();
                    }

                })
                .setNegativeButton(R.string.cancle, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        dialog.dismiss();

                    }
                })
                .create();

        return deleteMemberDialogBox;
    }

    //---------------------------------------------------------------------
    // now from here code start for custom dialog for adding new member
    //---------------------------------------------------------------


    protected void showCustomDialog() {

        final View popupForAddMember = LayoutInflater.from(getActivity()).inflate(R.layout.popup_add_member, null);
        final AlertDialog showDialog = new AlertDialog.Builder(getActivity()).create();
        showDialog.setView(popupForAddMember);
        initFindViewByIdForCustomDialog(popupForAddMember);

        IvContact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkContactPermission()) {
                    openContactList();
                } else {
                    requestContactPermission();
                }
            }
        });
        btnMemberSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                submitButtonOfAddMember();


                if (isValidEventName()) {
                    showDialog.dismiss();
                }

            }
        });
        showDialog.show();
    }


    // find id for custom dialogBox
    protected void initFindViewByIdForCustomDialog(View v) {
        etForMemberName = v.findViewById(R.id.etForAddMember);
        btnMemberSubmit = v.findViewById(R.id.btnMemberSubmit);
        IvContact = v.findViewById(R.id.IvContact);
        tvForShowTitle = v.findViewById(R.id.tvForShowTitleForAddMember);

    }


    //click event of submit button for adding new member
    protected void submitButtonOfAddMember() {
        if (!isMemberNameNotRepeat()) {
            if (isValidEventName()) {
                int groupid = new ActivityMain().getGroupId();
                long i = new TblMember(getActivity()).insertGroup(etForMemberName.getText().toString(), groupid);
                if (i > 0) {
                    memberList.clear();
                    setAdapterForShowingMemberList(groupid);
                    Toast.makeText(getActivity(), R.string.member_add_sucessfully, Toast.LENGTH_SHORT).show();
                    showBasicInfoOFMember(groupid);
                    LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(new Intent(Constant.BROADCAST_FOR_UPDATE_MEMBER_DETAIL_OF_TABLE));


                } else {
                    Toast.makeText(getActivity(), R.string.something_went_to_wrong, Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(getActivity(), R.string.error_enter_valid_name, Toast.LENGTH_LONG).show();
            }
        } else {
            Toast.makeText(getActivity(), R.string.member_already_exists, Toast.LENGTH_LONG).show();
            etForMemberName.setError(getString(R.string.error_event_name));

        }
    }


    //  open custom dialog when user need to update member information
    protected void showCustomDialog(final int position) {
        final MemberModel memberModel = memberList.get(position);
        final View popupForAddMember = LayoutInflater.from(getActivity()).inflate(R.layout.popup_add_member, null);
        final AlertDialog showDialog = new AlertDialog.Builder(getActivity()).create();
        showDialog.setView(popupForAddMember);
        initFindViewByIdForCustomDialog(popupForAddMember);
        showContactAppForAddMember();
        etForMemberName.setText(memberModel.getMemberName());
        btnMemberSubmit.setText(getString(R.string.btn_update));
        tvForShowTitle.setText(R.string.update_member);

        btnMemberSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (isValidEventName()) {
                    updateButtonForMember(memberModel.getMemberID(), etForMemberName.getText().toString());
                    showDialog.dismiss();
                }


            }
        });
        showDialog.show();
    }


    // logic for update member information
    protected void updateButtonForMember(int memberID, String updatedMemberName) {
        if (!isMemberNameNotRepeat()) {
            long lastUpdatedMember = new TblMember(getActivity()).updateUserById(memberID, updatedMemberName);
            long lastUpdatedMemberInTblTransaction = new TblTransaction(getActivity()).updateMemberNameByMemberId(memberID, updatedMemberName);
            Toast.makeText(getActivity(), " " + lastUpdatedMember + lastUpdatedMemberInTblTransaction, Toast.LENGTH_SHORT).show();
            if (lastUpdatedMember > 0) {
                memberList.clear();
                memberList.addAll(new TblMember(getActivity()).getMemberByID(groupId));
                memberAdapter.notifyDataSetChanged();
                Toast.makeText(getActivity(), R.string.member_updated_sucessfully, Toast.LENGTH_SHORT).show();
                LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(new Intent(Constant.BROADCAST_FOR_UPDATE_MEMBER_DETAIL_OF_TABLE));

            } else {
                Toast.makeText(getActivity(), R.string.something_went_to_wrong, Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(getActivity(), R.string.member_already_exists, Toast.LENGTH_LONG).show();

        }
    }

    // click event when user want to open contact application
    protected void showContactAppForAddMember() {
        IvContact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openContactList();
            }
        });
    }


    // result of choose member name from contact
    public void onActivityResult(int reqCode, int resultCode, Intent data) {
        super.onActivityResult(reqCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            Uri contactData = data.getData();
            Cursor c = getActivity().managedQuery(contactData, null, null, null, null);
            if (c.moveToFirst()) {
                String name = c.getString(c.getColumnIndexOrThrow(ContactsContract.Contacts.DISPLAY_NAME));
                etForMemberName.setText(name);
            }
        }
    }


    public void openContactList() {
        Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
        if (intent.resolveActivity(getActivity().getPackageManager()) != null) {
            startActivityForResult(intent, REQUEST_SELECT_CONTACT);
        } else {
            Toast.makeText(getActivity(), R.string.something_went_to_wrong, Toast.LENGTH_LONG).show();
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 1) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                openContactList();
            } else {
                Toast.makeText(getActivity(), R.string.permission_request_contact, Toast.LENGTH_LONG).show();
            }
        }

    }

    //=------------------------------------------------------\
    //some other utility method start from here like validation, permission request
    //--------------------------------------------------------\


    private void requestContactPermission() {
        String[] permissions = {Manifest.permission.READ_CONTACTS};
        ActivityCompat.requestPermissions(getActivity(), permissions, 1);
    }

    // check weather given member is repeating or not
    private boolean isMemberNameNotRepeat() {
        boolean isMemberNameNotRepeat = false;
        ArrayList<MemberModel> memberListTemp = new TblMember(getContext()).getMemberByID(groupId);
        if (memberListTemp.size() > 0) {
            for (int i = 0; i < memberListTemp.size(); i++) {
                if (etForMemberName.getText().toString().equals(memberListTemp.get(i).getMemberName())) {
                    isMemberNameNotRepeat = true;
                    break;
                }
            }
        }
        return isMemberNameNotRepeat;
    }

    // validation for name
    boolean isValidEventName() {
        boolean isValid = true;
        if (TextUtils.isEmpty(etForMemberName.getText().toString().trim())) {
            isValid = false;
            etForMemberName.setError(getString(R.string.error_event_name));
        }
        return isValid;
    }


    public boolean checkContactPermission() {
        boolean result = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_CONTACTS) == (PackageManager.PERMISSION_GRANTED);
        return result;
    }

}
