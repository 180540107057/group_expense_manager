package com.ketan.groupexpensemanager.util;

public class Constant {

    public static final String UPDATE_USER_INTENT_NAME = "updateUser";
    public static final String UPDATE_TRANSACTION_INTENT_NAME = "updateTransaction";
    public static final String BROADCAST_FOR_UPDATE_REPORT = "com.ketan.groupexpensemanager.sendBroadcastForUpdateReport";
    public static final String BROADCAST_FOR_UPDATE_MEMBER_DETAIL_OF_TABLE = "com.ketan.groupexpensemanager.sendBroadcastForUpdateMemberDetailOfTable";
    public static final String BROADCAST_FOR_UPDATE_TRANSACTION = "com.ketan.groupexpensemanager.sendBroadcastForUpdateTransaction";


}
